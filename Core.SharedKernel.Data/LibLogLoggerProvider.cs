﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibLog.Common.Logging;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore.Infrastructure;
using LogLevel = Microsoft.Extensions.Logging.LogLevel;

namespace Core.SharedKernel.Data
{
    public class LibLogLoggerProvider : ILoggerProvider
    {
        private readonly ILog _logger;

        public LibLogLoggerProvider(ILog logger)
        {
            _logger = logger;
        }

        public void Dispose()
        {
        }

        public ILogger CreateLogger(string categoryName)
        {
            return new LibLogLogger(_logger, categoryName);
        }

        private class LibLogLogger : ILogger
        {
            private readonly ILog _logger;
            private readonly string _categoryName;

            public LibLogLogger(ILog logger, string categoryName)
            {
                _logger = logger;
                _categoryName = categoryName;
            }

            public bool IsEnabled(LogLevel logLevel)
            {
                return true;
            }

            public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception exception,
                Func<TState, Exception, string> formatter)
            {
                //if (state is DbCommandLogData)  // obsolete soon...			// https://github.com/aspnet/EntityFramework/issues/6946
                if (eventId.Id == (int)RelationalEventId.ExecutedCommand)
                {
                    var commandText = formatter(state, exception);
                    switch (logLevel)
                    {
                        case LogLevel.Trace:
                            _logger.Trace(commandText);
                            break;

                        case LogLevel.Debug:
                            _logger.Debug(commandText);
                            break;

                        case LogLevel.None:
                        case LogLevel.Information:
                            _logger.Info(commandText);
                            break;

                        case LogLevel.Warning:
                            _logger.Warn(commandText);
                            break;

                        case LogLevel.Error:
                            _logger.Error(commandText);
                            break;

                        case LogLevel.Critical:
                            _logger.Fatal(commandText);
                            break;

                        default:
                            throw new Exception("unknown loglevel");
                    }
                }
            }

            public IDisposable BeginScope<TState>(TState state)
            {
                return null;
            }
        }
    }
}
