﻿using Autofac;
using AutoMapper;

namespace TeamFoodOrder.Bootstrapper
{
	public class AutoMapperConfig
	{
		public static void RegisterForDI(ContainerBuilder builder)
		{
			// http://stackoverflow.com/a/35208535/54159
			builder.Register(ctx => AutoMapperConfig.GetConfig());
			builder.Register(ctx => ctx.Resolve<MapperConfiguration>().CreateMapper()).As<IMapper>();
		}

		private static MapperConfiguration GetConfig()
		{
			var config = new MapperConfiguration(cfg =>
			{
				cfg.AddProfile<UncheckedAutoMapperConfig>();		// no call to AssertConfigurationIsValid below!
				cfg.AddProfile<CheckedAutoMapperConfig>();
				//cfg.CreateMissingTypeMaps = true;					// DynamicMap replacement?!?
			});
			config.AssertConfigurationIsValid(typeof(CheckedAutoMapperConfig).FullName);

			return config;
		}
	}

	internal class CheckedAutoMapperConfig : Profile
	{
		public CheckedAutoMapperConfig()
		{
			// ---------------------------------
			// simple view model mappings
			//CreateMap<OrderItem, ProductViewModel>()
			//	.ForSourceMember(s => s.IsDirty, o => o.Ignore())
			//    .ForMember(d => d.RequestedName, o => o.Ignore())
   //             .ForMember(d => d.IsFallBackMatch, o => o.Ignore())
			//	;

			//CreateMap<Link, LinkViewModel>()
			//	.ForSourceMember(s => s.IsDirty, o => o.Ignore())
			//	;

			//CreateMap<Tag, TagViewModel>()
			//	.ForSourceMember(s => s.IsDirty, o => o.Ignore())
			//	;

			// ---------------------------------
			// detail view models
			//CreateMap<OrderItem, ProductDetailViewModel>()
			//	.ForMember(d => d.LinkViewModels, o => o.ResolveUsing<ProductDetailsLinksResolver>())
			//	;

			//CreateMap<Link, LinkDetailViewModel>()
			//	.ForMember(d => d.TagViewModels, o => o.ResolveUsing<LinkDetailTagsResolver>())
			//	.ForMember(d => d.ProductViewModel, o => o.MapFrom(src => src.OrderItem))
			//	;

			//CreateMap<Tag, TagDetailViewModel>()
			//	.ForMember(d => d.LinkViewModels, o => o.ResolveUsing<TagDetailLinksResolver>())
			//	;
		}
	}

	internal class UncheckedAutoMapperConfig : Profile
	{
		public UncheckedAutoMapperConfig()
		{
			//CreateMap<ANLAGETEIL, AnlageData>()
			//    .ForMember(d => d.AnlageTeilId, o => o.MapFrom(src => src.anlageteil_ID))
			//    .ForMember(d => d.Name, o => o.MapFrom(src => src.teil_name))
			//    .ForMember(d => d.KurzZeichen, o => o.MapFrom(src => src.akz))
			//    ;
		}
	}
}
