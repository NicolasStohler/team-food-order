﻿using System;
using Autofac;
using EnsureThat;
using LibLog.Common.Logging;

namespace TeamFoodOrder.Bootstrapper
{
	public class Bootstrapper
	{
		private static readonly ILog Logger = LogProvider.GetCurrentClassLogger();

		private static IContainer _autofacContainer = null;

		public static IContainer Initialize(Action<ContainerBuilder> containerBuilderFn = null)
		{
			Logger.Info("Bootstrapper.Initialize");

			Ensure.That(_autofacContainer == null, "_autofacContainer").IsTrue();

			_autofacContainer = Bootstrapper.Initialize_internal(builder =>
			{
                // invoke containerBuilderFn if provided
			    containerBuilderFn?.Invoke(builder);

                AutoMapperConfig.RegisterForDI(builder);
				// QuartzAutofacConfig.RegisterForDI(builder);
			});

			Ensure.That(_autofacContainer, "_autofacContainer").IsNotNull();

			return _autofacContainer;
		}

		private static IContainer Initialize_internal(Action<ContainerBuilder> containerBuilder)
		{
			return AutofacLoader.Initialize(containerBuilder);
		}
	}
}