﻿using System;
using Autofac;
using AutofacRegistrationModule = TeamFoodOrder.Data.AutofacRegistrationModule;

namespace TeamFoodOrder.Bootstrapper
{
	public static class AutofacLoader
	{
		public static IContainer Initialize(Action<ContainerBuilder> containerBuilderFn)
		{
			IContainer container = null;
			ContainerBuilder builder = new ContainerBuilder();

			// register types here

			// using Autofac-Modules:
			builder.RegisterAssemblyModules(typeof(AutofacRegistrationModule).Assembly);
			builder.RegisterAssemblyModules(typeof(TeamFoodOrder.Engine.AutofacRegistrationModule).Assembly);
			builder.RegisterAssemblyModules(typeof(TeamFoodOrder.AppConfiguration.AutofacRegistrationModule).Assembly);

			// invoke containerBuilderFn if provided
			containerBuilderFn?.Invoke(builder);

			container = builder.Build();

			return container;
		}
	}
}