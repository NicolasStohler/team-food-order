﻿using System;

namespace TeamFoodOrder.Domain
{
	public class OrderItem : ClientChangeTracker
	{
        public int OrderItemId { get; set; }
		public string UserName { get; set; }
	    public string FoodName { get; set; }
	    public string Comment { get; set; }
	    public DateTime SubmitDateTime { get; set; }       
	}
}