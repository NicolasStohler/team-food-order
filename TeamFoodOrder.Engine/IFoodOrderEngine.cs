﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TeamFoodOrder.Domain;

namespace TeamFoodOrder.Engine
{
    public interface IFoodOrderEngine
    {
        Task<IEnumerable<OrderItem>> GetAllOrderItemsAsync();
        Task<IEnumerable<OrderItem>> GetOrderItemsByUserNameAsync(string userName);
        Task<OrderItem> GetOrderItemByIdAsync(int id);
        Task<OrderItem> InsertOrderItemAsync(OrderItem orderItem);
        Task<OrderItem> DeoleteOrderItemAsync(int id);
        Task<OrderItem> UpdateOrderItemAsync(int id, OrderItem orderItem);
    }
}