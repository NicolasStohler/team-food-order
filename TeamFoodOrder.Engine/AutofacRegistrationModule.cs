﻿using Autofac;

namespace TeamFoodOrder.Engine
{
	public class AutofacRegistrationModule : Module
	{
		protected override void Load(ContainerBuilder builder)
		{
			base.Load(builder);

			// builder.RegisterType<AppConfig>().As<IAppConfig>().InstancePerLifetimeScope();

			builder.RegisterAssemblyTypes(ThisAssembly)
				.AsImplementedInterfaces();

		}
	}
}
