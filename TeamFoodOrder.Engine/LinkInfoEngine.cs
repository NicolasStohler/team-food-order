﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DisconnectedGenericRepository;
using LibLog.Common.Logging;
using TeamFoodOrder.Data;
using TeamFoodOrder.Domain;
using TeamFoodOrder.ViewModels;

namespace TeamFoodOrder.Engine
{
    internal class LinkInfoEngine : ILinkInfoEngine
    {
        private static readonly ILog Logger = LogProvider.GetCurrentClassLogger();

        private readonly IDisconnectedData _disconnectedData;
        private readonly IGenericRepository<OrderItem> _productRepo;
        private readonly AutoMapper.IMapper _mapper;

        public LinkInfoEngine(IDisconnectedData disconnectedData, IGenericRepository<OrderItem> productRepo,
            IMapper mapper)
        {
            _disconnectedData = disconnectedData;
            _productRepo = productRepo;
            _mapper = mapper;
        }

        private ProductViewModel GetProductViewModel(OrderItem orderItem, string requestedName)
        {
            if (orderItem == null)
                return null;

            var productViewModel = _mapper.Map<ProductViewModel>(orderItem);
            productViewModel.RequestedName = requestedName;
            productViewModel.IsFallBackMatch =
                (!productViewModel.Name.Equals(requestedName, StringComparison.CurrentCultureIgnoreCase));

            return productViewModel;
        }

        // ASYNC
        public async Task<IEnumerable<ProductViewModel>> GetAllProductsAsync()
        {
            var productViewModels = (await _productRepo.AllAsync())
                .Select(p => _mapper.Map<ProductViewModel>(p))
                .ToList();
            return productViewModels;
        }

        public async Task<ProductViewModel> GetProductViewModelAsync(string name, string dbType = "production")
        {
            // ext: name parsing: "iasibe2-formulare-frm123" 
            // 1: exists "iasibe2-formulare-frm123": yes => return like above
            // 2: exists "iasibe2-formulare": yes => return like above
            // 3: exists "iasibe2": yes => return like above
            // return null

            var parts = name.Trim().Split('-');
            for (int useParts = parts.Count(); useParts > 0; useParts--)
            {
                var searchName = string.Join("-", parts.Take(useParts));
                var product = await GetProductByNameAsync(searchName, dbType);
                if (product != null)
                {
                    return GetProductViewModel(product, name);
                }
            }

            return null;
        }

        private async Task<OrderItem> GetProductByNameAsync(string name, string dbType = "production")
        {
            throw new NotImplementedException();
            //// const string defaultDbType = "production";
            //var today = DateTime.Today;

            //// advanced: take "production", where validTo == null
            //var product = (await _productRepo
            //        .FindByAsync(p =>
            //            p.Name == name &&
            //            p.DbType == dbType &&
            //            ((p.ValidFrom <= today && p.ValidTo == null) || (p.ValidFrom <= today && today < p.ValidTo))
            //        ))
            //    .SingleOrDefault();
            //return product;
        }

        public async Task<ProductViewModel> GetProductViewModelByProductIdAsync(int productId)
        {
            var product = (await _productRepo
                    .FindByAsync(p => p.OrderItemId == productId))
                .SingleOrDefault();

            return GetProductViewModel(product, $"OrderItemId = {productId}");
        }

        public async Task<IEnumerable<ProductViewModel>> GetRelatedProductViewModelsAsync(string name)
        {
            var products = await GetRelatedProductsAsync(name);
            return products.Select(p => GetProductViewModel(p, name)).ToList();
        }

        private async Task<IEnumerable<OrderItem>> GetRelatedProductsAsync(string name)
        {
            var offlineProducts = new List<OrderItem>();
            ;
            var parts = name.Trim().Split('-');
            for (int useParts = parts.Count(); useParts > 0; useParts--)
            {
                var searchName = string.Join("-", parts.Take(useParts));
                var products = await GetRelatedByNameAsync(searchName);
                if (products != null)
                {
                    offlineProducts.AddRange(products);
                }
            }
            return offlineProducts;
        }

        private async Task<IEnumerable<OrderItem>> GetRelatedByNameAsync(string name)
        {
            throw new NotImplementedException();
            //var products = await _productRepo.FindByAsync(p => p.Name == name);
            //return products;
        }
    }
}
