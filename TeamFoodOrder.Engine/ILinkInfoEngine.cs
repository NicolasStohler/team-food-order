﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TeamFoodOrder.ViewModels;

namespace TeamFoodOrder.Engine
{
	public interface ILinkInfoEngine
	{
	    Task<IEnumerable<ProductViewModel>> GetAllProductsAsync();
	    Task<ProductViewModel> GetProductViewModelAsync(string name, string dbType = "production");
	    Task<ProductViewModel> GetProductViewModelByProductIdAsync(int productId);
	    Task<IEnumerable<ProductViewModel>> GetRelatedProductViewModelsAsync(string name);
	}
}