﻿using System;
using System.Linq;
using AutoMapper;
using DisconnectedGenericRepository;
using LibLog.Common.Logging;
using TeamFoodOrder.AppConfiguration;
using TeamFoodOrder.Data;
using TeamFoodOrder.Domain;

namespace TeamFoodOrder.Engine
{
	internal class DemoEngine : IDemoEngine
	{
		private static readonly ILog Logger = LogProvider.GetCurrentClassLogger();

		private readonly IAppConfig _appConfig;
		private readonly IDisconnectedData _disconnectedData;
	    private readonly IGenericRepository<OrderItem> _productRepo;
		private readonly AutoMapper.IMapper _mapper;

		public DemoEngine(IAppConfig appConfig,
			IDisconnectedData disconnectedData,
			IGenericRepository<OrderItem> productRepo, 
			IMapper mapper)
		{
			_disconnectedData = disconnectedData;
		    _productRepo = productRepo;
			_mapper = mapper;
			_appConfig = appConfig;
		}

		public void Execute()
		{
			Logger.Info("Hello from DemoEngine!");

			// AppConfig
			Logger.Info("AppConfig:");
			Logger.InfoFormat("  - MySetting = {mySetting}", _appConfig.MySetting);
			Logger.InfoFormat("  - MySetting = {myValue}", _appConfig.MyValue);
			Logger.InfoFormat("  - MySetting = {myDate}", _appConfig.MyDate.ToShortDateString());
            
			// _productRepo
			Logger.Info("_productRepo");

			var newProduct = new OrderItem() { FoodName = $"Demo-{Guid.NewGuid()}", IsDirty = true };
			// _productRepo.Insert(newProduct);

			var products = _productRepo.All(); 
			foreach (var product in products)
			{
				Logger.InfoFormat("- OrderItem: {name}", product.FoodName);
			}

			// test automapper
			//var firstLink = _disconnectedData.GetLinkById(1);
			//Ensure.That(firstLink.LinkTags).HasItems();
			//var sourceCount = firstLink.LinkTags.Count();
			////var firstLink = links.First();
			//var linkVm = _mapper.Map<LinkViewModel>(firstLink);
			////Ensure.That(linkVm.TagViewModels).HasItems();
			////Ensure.That(linkVm.TagViewModels.Count).Is(sourceCount);

			var productId = 1;
			var productWithLinks = _productRepo
				.FindBy(p => p.OrderItemId == productId)
				.SingleOrDefault();

            //var productWithLinksVm = _mapper.Map<ProductDetailViewModel>(productWithLinks);
			//Ensure.That(productWithLinksVm.LinkViewModels).HasItems();

			//var linkDetailsVm = _mapper.Map<LinkDetailViewModel>(firstLink);

			//// var tagWithLinks = _tagRepo.FindByInclude(t => t.Name == "search", t => t.LinkTags).SingleOrDefault();
			//var tagWithLinks = _disconnectedData.GetTagWithLinks("search");
			//var tagDetailsVm = _mapper.Map<TagDetailViewModel>(tagWithLinks);


		}
	}
}
