﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DisconnectedGenericRepository;
using LibLog.Common.Logging;
using TeamFoodOrder.Domain;

namespace TeamFoodOrder.Engine
{
    internal class FoodOrderEngine : IFoodOrderEngine
    {
        private static readonly ILog Logger = LogProvider.GetCurrentClassLogger();

        private readonly IGenericRepository<OrderItem> _orderItemRepo;
        private readonly AutoMapper.IMapper _mapper;

        public FoodOrderEngine(IGenericRepository<OrderItem> orderItemRepo, IMapper mapper)
        {
            _orderItemRepo = orderItemRepo;
            _mapper = mapper;
        }

        public async Task<IEnumerable<OrderItem>> GetAllOrderItemsAsync()
        {
            var orderItems = await _orderItemRepo.AllAsync();
            return orderItems;
        }

        public async Task<IEnumerable<OrderItem>> GetOrderItemsByUserNameAsync(string userName)
        {
            var orderItems = await _orderItemRepo.FindByAsync(o => o.UserName == userName);
            return orderItems;
        }

        public async Task<OrderItem> GetOrderItemByIdAsync(int id)
        {
            var orderItems = await _orderItemRepo.FindByAsync(o => o.OrderItemId == id);
            if (orderItems.Count() == 1)
                return orderItems.First();

            return null;
        }

        public async Task<OrderItem> InsertOrderItemAsync(OrderItem orderItem)
        {
            await _orderItemRepo.InsertAsync(orderItem);
            return orderItem;
        }

        public async Task<OrderItem> DeoleteOrderItemAsync(int id)
        {
            var orderItem = await _orderItemRepo.DeleteAsync(id);
            return orderItem;
        }

        public async Task<OrderItem> UpdateOrderItemAsync(int id, OrderItem orderItem)
        {
            orderItem.OrderItemId = id;
            await _orderItemRepo.UpdateAsync(orderItem);
            return orderItem;
        }
    }
}
