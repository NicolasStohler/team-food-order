﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using TeamFoodOrder.Domain;

namespace TeamFoodOrder.Data
{
	internal class DisconnectedData : IDisconnectedData
	{
		private readonly TeamFoodOrderContext _context;

		public DisconnectedData(TeamFoodOrderContext context)
		{
			_context = context;

		    // http://thedatafarm.com/data-access/ef-core-lets-us-finally-define-notracking-dbcontexts/
            _context.ChangeTracker.QueryTrackingBehavior        // no tracking at all! no more .AsNoTracking() required
			  = QueryTrackingBehavior.NoTracking;               // BUT: when tracking is required, use: context.Samurais.AsTracking().ToList();
		}

		//public Link GetLinkById(int linkId)
		//{
		//	// https://docs.microsoft.com/en-us/ef/core/querying/related-data

		//	var link = _context.Links
		//		.Where(l => l.LinkId == linkId)
		//		.Include(l => l.LinkTags)
		//		.ThenInclude(lt => lt.Tag)
		//		.Include(l => l.OrderItem)
		//		.SingleOrDefault();

		//	return link;
		//}

		//public List<Link> GetLinksByTag(string tagName)
		//{
		//	var tag = _context.Tags.SingleOrDefault(t => t.Name == tagName);
		//	if (tag != null)
		//	{
		//		//var test = _context.LinkTags
		//		//	.Where(lt => lt.TagId == tag.Id)
		//		//	.Include(lt => lt.Link)
		//		//	.Include(lt => lt.Tag)
		//		//	.ToList();

		//		//var test2 = _context.Entry(tag)
		//		//	.Collection(t => t.LinkTags).Query().Include(lt => lt.Link)
		//		//	.Include(lt => lt.Tag)
		//		//	.ToList();

		//		//// standard join ... 
		//		//var test3 = from lt in _context.LinkTags
		//		//			join t in _context.Tags on lt.TagId equals t.Id
		//		//			where t.Id == tag.Id
		//		//			select new { lt, t };

		//		//var t3 = test3.ToList();

		//		//// lets try again
		//		//// from tag, get links that contain the tag:
		//		//var linksForTag = _context.LinkTags
		//		//		.Where(lt => lt.TagId == tag.Id)
		//		//		.Select(lt => lt.Link)
		//		//	//.ToList();	// remove later? 
		//		//	;

		//		//// now for those links, get all tags (not just the one provided)
		//		//// - try without include first
		//		//var completeLinks = from l in linksForTag
		//		//					join lt in _context.LinkTags on l.Id equals lt.LinkId
		//		//					join t in _context.Tags on lt.TagId equals t.Id
		//		//					select l
		//		//	;

		//		//var final = completeLinks
		//		//	.Include(l => l.LinkTags)
		//		//	.ThenInclude(lt => lt.Tag)
		//		//	.ToList();




		//		//var links = _context.Entry(tag).Collection(t => t.)
		//		var links = _context.LinkTags
		//			.Where(lt => lt.TagId == tag.TagId)
		//			.Include(lt => lt.Link)
		//			.Select(lt => lt.Link)
		//			.ToList();
		//		//.Load();

		//		return links;
		//	}
		//    return null;
		//}

		//public Link AddLink(Link link)
		//{
		//	_context.ChangeTracker.TrackGraph
		//	  (link, e => ApplyStateUsingIsKeySet(e.Entry));
		//	_context.SaveChanges();
		//	return link;
		//}

		//public List<Link> GetAllLinks()
		//{
		//	var links = _context.Links.ToList();
		//	return links;
		//}

		//public List<Tag> GetTagsForLink(int linkId)
		//{
		//	var tags = _context.LinkTags
		//		.Where(lt => lt.LinkId == linkId)
		//		.Include(lt => lt.Tag)
		//		.Select(lt => lt.Tag)
		//		.ToList();

		//	return tags;
		//}

		//public Tag GetTagDetails(int tagId)
		//{
		//	var tag = _context.Tags.SingleOrDefault(t => t.TagId == tagId);
		//	if (tag != null)
		//	{
		//		// load links for tag
		//		tag.LinkTags = _context.LinkTags
		//			.Where(lt => lt.TagId == tag.TagId)
		//			.Include(lt => lt.Link)
		//			.ToList();
		//	}
		//	return tag;
		//}

		//public Tag GetTagWithLinks(string tagName)
		//{
		//	var tag = _context.Tags.SingleOrDefault(t => t.Name == tagName);
		//	if (tag != null)
		//	{
		//		// load links for tag
		//		tag.LinkTags = _context.LinkTags
		//			.Where(lt => lt.TagId == tag.TagId)
		//			.Include(lt => lt.Link)
		//			.ToList();
		//	}
		//	return tag;
		//}

		//public List<KeyValuePair<int, Link>> GetLinksByTags(List<string> tags)
		//{
		//	throw new NotImplementedException();
		//}

		////public List<KeyValuePair<int, string>> GetSamuraiReferenceList()
		////{
		////	var samurais = _context.Samurais.OrderBy(s => s.Name)
		////	  .Select(s => new { s.Id, s.Name })
		////	  .ToDictionary(t => t.Id, t => t.Name).ToList();
		////	return samurais;
		////}

		////public Samurai LoadSamuraiGraph(int id)
		////{
		////	var samurai =
		////	  _context.Samurais
		////	  .Include(s => s.SecretIdentity)
		////	  .Include(s => s.Quotes)
		////	  .FirstOrDefault(s => s.Id == id);
		////	return samurai;
		////}

		////public void SaveSamuraiGraph(Samurai samurai)
		////{
		////	_context.ChangeTracker.TrackGraph
		////	  (samurai, e => ApplyStateUsingIsKeySet(e.Entry));
		////	_context.SaveChanges();
		////}

		private static void ApplyStateUsingIsKeySet(EntityEntry entry)
		{
			if (entry.IsKeySet)
			{
				if (((ClientChangeTracker)entry.Entity).IsDirty)
				{
					entry.State = EntityState.Modified;
				}
				else
				{
					entry.State = EntityState.Unchanged;
				}
			}
			else
			{
				entry.State = EntityState.Added;
			}
		}


		//public void DeleteSamuraiGraph(int id)
		//{
		//	//goal:  delete samurai , quotes and secret identity
		//	//       also delete any joins with battles
		//	//EF Core supports Cascade delete by convention
		//	//Even if full graph is not in memory, db is defined to delete
		//	//But always double check!
		//	var samurai = _context.Samurais.Find(id); //NOT TRACKING !!
		//	_context.Entry(samurai).State = EntityState.Deleted; //TRACKING
		//	_context.SaveChanges();
		//}
	}
}
