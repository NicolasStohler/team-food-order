﻿using System.Linq;
using Autofac;
using DisconnectedGenericRepository;
using Microsoft.EntityFrameworkCore;

namespace TeamFoodOrder.Data
{
	public class AutofacRegistrationModule : Module
	{
		protected override void Load(ContainerBuilder builder)
		{
			base.Load(builder);

			// this works too:
			//builder.RegisterAssemblyTypes(ThisAssembly)
			//	.AsImplementedInterfaces();

			// ...but as an explicit registration example:
			builder.RegisterAssemblyTypes(ThisAssembly)
				.Where(t => t.Name.EndsWith("Repository"))
				.As(t => t.GetInterfaces().FirstOrDefault(i => i.Name == "I" + t.Name));

			builder.RegisterType<TeamFoodOrderContext>()   // provide a TeamFoodOrderContext
                .As<DbContext>()					// - when a DbContext is requested
				.AsSelf()							// - or when a TeamFoodOrderContext is requested 
				.InstancePerLifetimeScope()			// and always return the same instance (per request in WebApi, singleton in console app)
				;

			builder.RegisterType<DisconnectedData>().As<IDisconnectedData>();
			// builder.RegisterType<DataRepositoryFactory>().As<IDataRepositoryFactory>();

			builder.RegisterGeneric(typeof(GenericRepository<>))
				.As(typeof(IGenericRepository<>))
				//.WithParameter(new TypedParameter(typeof(DbContext), new TestDbContext()))	// DEMO!
				;   // interface!
					//.AsSelf();

		}
	}
}
