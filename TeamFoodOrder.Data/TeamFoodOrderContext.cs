﻿using System;
using System.Configuration;
using System.Linq;
using Core.SharedKernel.Data;
using LibLog.Common.Logging;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using TeamFoodOrder.AppConfiguration;
using TeamFoodOrder.Domain;

namespace TeamFoodOrder.Data
{
	internal class TeamFoodOrderContext : DbContext
	{
		private static readonly ILog Logger = LogProvider.GetCurrentClassLogger();

		private readonly IDbContextConfig _dbContextConfig;

		public TeamFoodOrderContext(DbContextOptions<TeamFoodOrderContext> options, IDbContextConfig dbContextConfig)
			: base(options)
		{
            _dbContextConfig = dbContextConfig;
            // http://thedatafarm.com/data-access/ef-core-lets-us-finally-define-notracking-dbcontexts/
		    ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
		}

		public TeamFoodOrderContext(IDbContextConfig dbContextConfig)
		{
            _dbContextConfig = dbContextConfig;
		    // http://thedatafarm.com/data-access/ef-core-lets-us-finally-define-notracking-dbcontexts/
		    ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
        }

		public DbSet<OrderItem> OrderItems { get; set; }
		// public DbSet<User> Users { get; set; }
		
		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
		{
            if (!optionsBuilder.IsConfigured)
			{
				var settings = ConfigurationManager.ConnectionStrings;
				var connectionString = settings["TeamFoodOrderDb"].ConnectionString;

				optionsBuilder.UseSqlServer(connectionString,
											options => options.MaxBatchSize(30));

				if (_dbContextConfig.EnableSensitiveDataLogging)
				{
					optionsBuilder.EnableSensitiveDataLogging(); // display parameter values in logs	
				}

				if (_dbContextConfig.AddLibLogLoggerProvider)
				{
					LoggerFactory loggerFactory = new LoggerFactory();
					var libLogLoggerProvider = new LibLogLoggerProvider(Logger);
					loggerFactory.AddProvider(libLogLoggerProvider);
					optionsBuilder.UseLoggerFactory(loggerFactory);
				}
			}

			//optionsBuilder.UseSqlServer(
			//  "Server = (localdb)\\mssqllocaldb; Database = SamuraiDataCoreWeb; Trusted_Connection = True; ",
			//  options => options.MaxBatchSize(30));
			//optionsBuilder.EnableSensitiveDataLogging();
		}

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			//modelBuilder.HasDefaultSchema("connstring");

			//modelBuilder.Entity<LinkTag>()
			//	.HasKey(s => new { s.LinkId, s.TagId });

			//modelBuilder.Entity<Tag>()
			//	.HasIndex(t => t.Name).IsUnique();

			foreach (var entityType in modelBuilder.Model.GetEntityTypes())
			{
				modelBuilder.Entity(entityType.Name).Property<DateTime>("LastModified");
				modelBuilder.Entity(entityType.Name).Ignore("IsDirty");
			}
		}

		public override int SaveChanges()
		{
			foreach (var entry in ChangeTracker.Entries()
			 .Where(e => e.State == EntityState.Added ||
						 e.State == EntityState.Modified))
			{
				entry.Property("LastModified").CurrentValue = DateTime.Now;
			}
			return base.SaveChanges();
		}
	}
}
