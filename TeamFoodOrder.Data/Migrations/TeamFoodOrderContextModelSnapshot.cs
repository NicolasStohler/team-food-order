﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using TeamFoodOrder.Data;

namespace TeamFoodOrder.Data.Migrations
{
    [DbContext(typeof(TeamFoodOrderContext))]
    partial class TeamFoodOrderContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.1")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("TeamFoodOrder.Domain.OrderItem", b =>
                {
                    b.Property<int>("OrderItemId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Comment");

                    b.Property<string>("FoodName");

                    b.Property<DateTime>("LastModified");

                    b.Property<DateTime>("SubmitDateTime");

                    b.Property<string>("UserName");

                    b.HasKey("OrderItemId");

                    b.ToTable("OrderItems");
                });
        }
    }
}
