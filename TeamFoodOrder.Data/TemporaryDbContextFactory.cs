﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using TeamFoodOrder.AppConfiguration;

namespace TeamFoodOrder.Data
{
	internal class TemporaryDbContextFactory : IDbContextFactory<TeamFoodOrderContext>
	{
		public TeamFoodOrderContext Create(DbContextFactoryOptions options)
		{
			var builder = new DbContextOptionsBuilder<TeamFoodOrderContext>();
			builder.UseSqlServer(@"Server = (localdb)\mssqllocaldb; Database = TeamFoodOrder; Trusted_Connection = True;");

			IDbContextConfig dummyDbContextConfig = new DbContextConfigDummy();

			return new TeamFoodOrderContext(builder.Options, dummyDbContextConfig);
		}
	}
}