using System;

namespace TeamFoodOrder.ViewModels
{
	public class ProductViewModel
	{
		public int ProductId { get; set; }
		public string Name { get; set; }
	    public string ConnectionString { get; set; }
	    public DateTime ValidFrom { get; set; }
	    public DateTime? ValidTo { get; set; }
	    public string DbType { get; set; }
        
        // request processing info:
	    public string RequestedName { get; set; }
	    public bool IsFallBackMatch { get; set; }

	    //public string GetCrc32()
	    //{
	    //    var propertyNames = OrderItem.Crc32PropertyNames;

	    //    var sb = new StringBuilder();
	    //    var properties = this.GetType()
	    //        .GetProperties()
	    //        .Where(p => propertyNames.Contains(p.Name))
	    //        .ToList();

	    //    foreach (var propertyInfo in properties)
	    //    {
	    //        var value = propertyInfo.GetValue(this, null);
	    //        var valueAsString = value?.ToString();
	    //        sb.Append($"{propertyInfo.Name}:{valueAsString}|");
	    //    }
	    //    var bytes = Encoding.ASCII.GetBytes(sb.ToString());
	    //    var crc32 = Crc32Algorithm.Compute(bytes);
	    //    var crc32Text = crc32.ToString("x8");
	    //    return crc32Text;
	    //}
    }
}