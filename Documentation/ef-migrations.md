# Entity Framework Core Migrations

## Start from scratch

1. In Visual Studio, delete the `Database\ConnString.Data\Migrations` folder
- Set `Database\ConnString.Data` as **startup project**
- Open the **Package Manager Console**
  1. Default project: `Database\ConnString.Data`
  - `Add-Migration Init`: Creates the first migration
  - `Update-Database`: Creates the database (using the connection string hardcoded in **TemporaryDbContextFactory.cs**)

> Note: The database can be dropped using `Drop-Database`
