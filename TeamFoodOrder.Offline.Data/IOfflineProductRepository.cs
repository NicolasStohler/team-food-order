﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ConnString.Offline.Domain;

namespace ConnString.Offline.Data
{
    public interface IOfflineProductRepository
    {
        Task EnsureDatabaseExistsAsync();
        Task<OfflineProduct> GetOfflineProductAsync(string name, string dbType);
        Task<OfflineProduct> AddOrUpdateOfflineProductAsync(OfflineProduct offlineProduct);
        Task<IEnumerable<OfflineProduct>> GetRelatedOfflineProductsAsync(string name);
        Task AddOrUpdateRelatedOfflineProductsAsync(IEnumerable<OfflineProduct> relatedOfflineProducts);
    }
}