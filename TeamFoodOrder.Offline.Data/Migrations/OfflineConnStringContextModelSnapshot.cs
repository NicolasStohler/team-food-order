﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using ConnString.Offline.Data;

namespace ConnString.Offline.Data.Migrations
{
    [DbContext(typeof(OfflineConnStringContext))]
    partial class OfflineConnStringContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.1");

            modelBuilder.Entity("ConnString.Offline.Domain.OfflineProduct", b =>
                {
                    b.Property<int>("OfflineProductId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ConnectionString");

                    b.Property<string>("Crc32");

                    b.Property<string>("DbType");

                    b.Property<DateTime>("LastModified");

                    b.Property<string>("Name");

                    b.Property<DateTime>("OfflineValidUntil");

                    b.Property<int>("ProductId");

                    b.Property<DateTime>("ValidFrom");

                    b.Property<DateTime?>("ValidTo");

                    b.HasKey("OfflineProductId");

                    b.HasIndex("ProductId")
                        .IsUnique();

                    b.ToTable("OfflineProducts");
                });
        }
    }
}
