﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ConnString.Offline.Data.Migrations
{
    public partial class Init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "OfflineProducts",
                columns: table => new
                {
                    OfflineProductId = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    ConnectionString = table.Column<string>(nullable: true),
                    Crc32 = table.Column<string>(nullable: true),
                    DbType = table.Column<string>(nullable: true),
                    LastModified = table.Column<DateTime>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    OfflineValidUntil = table.Column<DateTime>(nullable: false),
                    ProductId = table.Column<int>(nullable: false),
                    ValidFrom = table.Column<DateTime>(nullable: false),
                    ValidTo = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OfflineProducts", x => x.OfflineProductId);
                });

            migrationBuilder.CreateIndex(
                name: "IX_OfflineProducts_ProductId",
                table: "OfflineProducts",
                column: "ProductId",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "OfflineProducts");
        }
    }
}
