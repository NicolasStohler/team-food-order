﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConnString.Offline.Domain;
using Microsoft.EntityFrameworkCore;

namespace ConnString.Offline.Data
{
    internal class OfflineProductRepository : IOfflineProductRepository
    {
        private readonly OfflineConnStringContext _offlineConnStringContext;

        public OfflineProductRepository(OfflineConnStringContext offlineConnStringContext)
        {
            _offlineConnStringContext = offlineConnStringContext;
        }

        public async Task EnsureDatabaseExistsAsync()
        {
            await _offlineConnStringContext.EnsureDatabaseExistsAsync();
        }

        public async Task<OfflineProduct> GetOfflineProductAsync(string name, string dbType)
        {
            var today = DateTime.Today;

            return await _offlineConnStringContext.OfflineProducts
                .SingleOrDefaultAsync(o =>
                    o.Name == name &&
                    o.DbType == dbType &&
                    ((o.ValidFrom <= today && o.ValidTo == null) || (o.ValidFrom <= today && today < o.ValidTo))
                );
        }

        public async Task<IEnumerable<OfflineProduct>> GetRelatedOfflineProductsAsync(string name)
        {
            // any DbType, any validity date range
            return await _offlineConnStringContext.OfflineProducts
                .Where(o => o.Name == name)
                .ToListAsync();
        }

        public async Task AddOrUpdateRelatedOfflineProductsAsync(IEnumerable<OfflineProduct> relatedOfflineProducts)
        {
            // http://stackoverflow.com/a/26069550/54159
            var tasks = relatedOfflineProducts.Select(o => AddOrUpdateOfflineProductAsync(o)).ToList();
            await Task.WhenAll(tasks);

            //foreach (var relatedOfflineProduct in relatedOfflineProducts)
            //{
            //    await AddOrUpdateOfflineProductAsync(relatedOfflineProduct);
            //}
        }

        public async Task<OfflineProduct> AddOrUpdateOfflineProductAsync(OfflineProduct offlineProduct)
        {
            // add or update: check on productId, existing? -> update, insert otherwise
            var existingRec = await _offlineConnStringContext.OfflineProducts
                .SingleOrDefaultAsync(o => o.ProductId == offlineProduct.ProductId);

            if (existingRec != null)
            {
                if (existingRec.Crc32 != offlineProduct.Crc32)
                {
                    // http://kerryritter.com/updating-or-replacing-entities-in-entity-framework-6/
                    _offlineConnStringContext.Entry(existingRec).CurrentValues.SetValues(offlineProduct);
                }
            }
            else
            {
                await _offlineConnStringContext.OfflineProducts.AddAsync(offlineProduct);
            }

            await _offlineConnStringContext.SaveChangesAsync();
            return offlineProduct;
        }
    }
}
