﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ConnString.AppConfiguration;
using ConnString.Offline.Domain;
using Core.SharedKernel.Data;
using LibLog.Common.Logging;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.Extensions.Logging;

namespace ConnString.Offline.Data
{
    internal class OfflineConnStringContext : DbContext
    {
        private static readonly ILog Logger = LogProvider.GetCurrentClassLogger();

        private readonly IDbContextConfig _dbContextConfig;

        public OfflineConnStringContext(DbContextOptions<OfflineConnStringContext> options, IDbContextConfig dbContextConfig)
            : base(options)
        {
            _dbContextConfig = dbContextConfig;

            // http://thedatafarm.com/data-access/ef-core-lets-us-finally-define-notracking-dbcontexts/
            ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
        }

        public OfflineConnStringContext(IDbContextConfig dbContextConfig)
        {
            _dbContextConfig = dbContextConfig;

            // http://thedatafarm.com/data-access/ef-core-lets-us-finally-define-notracking-dbcontexts/
            ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
        }

        public DbSet<OfflineProduct> OfflineProducts { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                var settings = ConfigurationManager.ConnectionStrings;
                var connectionString = settings["OfflineConnStringDb"].ConnectionString;

                //var connectionString = @"Data Source=C:\Temp\ConnStringOffline.db";

                optionsBuilder.UseSqlite(connectionString,
                    options => options.MaxBatchSize(30));

                if (_dbContextConfig.EnableSensitiveDataLogging)
                {
                    optionsBuilder.EnableSensitiveDataLogging(); // display parameter values in logs	
                }

                if (_dbContextConfig.AddLibLogLoggerProvider)
                {
                    LoggerFactory loggerFactory = new LoggerFactory();
                    var libLogLoggerProvider = new LibLogLoggerProvider(Logger);
                    loggerFactory.AddProvider(libLogLoggerProvider);
                    optionsBuilder.UseLoggerFactory(loggerFactory);
                }
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //modelBuilder.HasDefaultSchema("connstring");

            //modelBuilder.Entity<LinkTag>()
            //	.HasKey(s => new { s.LinkId, s.TagId });

            modelBuilder.Entity<OfflineProduct>()
            	.HasIndex(t => t.ProductId).IsUnique();

            foreach (var entityType in modelBuilder.Model.GetEntityTypes())
            {
                modelBuilder.Entity(entityType.Name).Property<DateTime>("LastModified");
                modelBuilder.Entity(entityType.Name).Ignore("IsDirty");
            }
        }

        private void ProcessSaveChangesLastModified()
        {
            foreach (var entry in ChangeTracker.Entries()
                .Where(e => e.State == EntityState.Added ||
                            e.State == EntityState.Modified))
            {
                entry.Property("LastModified").CurrentValue = DateTime.Now;
            }
        }

        public override int SaveChanges()
        {
            ProcessSaveChangesLastModified();
            return base.SaveChanges();
        }

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken())
        {
            ProcessSaveChangesLastModified();
            return base.SaveChangesAsync(cancellationToken);
        }

        public override int SaveChanges(bool acceptAllChangesOnSuccess)
        {
            ProcessSaveChangesLastModified();
            return base.SaveChanges(acceptAllChangesOnSuccess);
        }

        public override Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = new CancellationToken())
        {
            ProcessSaveChangesLastModified();
            return base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
        }
        
        public async Task EnsureDatabaseExistsAsync()
        {
            try
            {
                await this.Database.MigrateAsync();
            }
            catch (Exception ex)
            {
                Logger.WarnException("Db-Migration failed...", ex);
                await RecreateDatabaseAsync();
            }
        }

        private async Task RecreateDatabaseAsync()
        {
            try
            {
                Logger.Info("Trying to recreate the database");
                await this.Database.EnsureDeletedAsync();
                await this.Database.MigrateAsync();
                Logger.Info("Recreate the database: success!");
            }
            catch (Exception ex)
            {
                Logger.ErrorException("Db recreation failure!", ex);
                throw;
            }
        }
    }
}
