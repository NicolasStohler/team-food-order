﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConnString.AppConfiguration;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;

namespace ConnString.Offline.Data
{
    internal class TemporaryDbContextFactory : IDbContextFactory<OfflineConnStringContext>
    {
        public OfflineConnStringContext Create(DbContextFactoryOptions options)
        {
            var builder = new DbContextOptionsBuilder<OfflineConnStringContext>();
            builder.UseSqlite(@"Data Source=C:\Temp\ConnStringOffline.db");

            IDbContextConfig dummyDbContextConfig = new DbContextConfigDummy();

            return new OfflineConnStringContext(builder.Options, dummyDbContextConfig);
        }
    }
}
