﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using DisconnectedGenericRepository;
using Microsoft.EntityFrameworkCore;

namespace ConnString.Offline.Data
{
    public class AutofacRegistrationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            // this works too:
            //builder.RegisterAssemblyTypes(ThisAssembly)
            //	.AsImplementedInterfaces();

            // ...but as an explicit registration example:
            builder.RegisterAssemblyTypes(ThisAssembly)
                .Where(t => t.Name.EndsWith("Repository"))
                .As(t => t.GetInterfaces().FirstOrDefault(i => i.Name == "I" + t.Name));

            builder.RegisterType<OfflineConnStringContext>()   // provide a ConnStringContext
                .As<DbContext>()					// - when a DbContext is requested
                .AsSelf()                           // - or when a ConnStringContext is requested 
                .AsImplementedInterfaces()
                //.InstancePerLifetimeScope()         // and always return the same instance (per request in WebApi, singleton in console app)
                ;

            builder.RegisterGeneric(typeof(GenericRepository<>))
                .As(typeof(IGenericRepository<>))
                //.WithParameter(new TypedParameter(typeof(DbContext), new TestDbContext()))	// DEMO!
                ;   // interface!
            //.AsSelf();

        }
    }
}
