﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConnString.SqliteTest.App
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var context = new LiteContext())
            {
                context.EnsureDatabaseExists();

                var products = context.Products.ToList();

                context.Products.Add(new Product() { Name = "test" });
                context.SaveChanges();

            }
        }
    }
}
