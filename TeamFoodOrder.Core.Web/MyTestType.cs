﻿namespace TeamFoodOrder.Core.Web
{
    internal class MyTestType : IMyTestType
    {
        public string Name { get; set; }

        public string GetDevName()
        {
            System.Diagnostics.Debug.Print("hello MyTestType");
            return "Nicolas Stohler";
        }
    }
}