﻿using Microsoft.AspNetCore.Mvc;

namespace TeamFoodOrder.Core.Web.Controllers.Web
{
    public class HomeController : Controller
    {
        private readonly IMyTestType _myTestType;

        public HomeController(IMyTestType myTestType)
        {
            _myTestType = myTestType;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}
