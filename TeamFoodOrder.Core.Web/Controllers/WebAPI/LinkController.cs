using LibLog.Common.Logging;
using Microsoft.AspNetCore.Mvc;
using TeamFoodOrder.Engine;

namespace TeamFoodOrder.Core.Web.Controllers.WebAPI
{
    [Produces("application/json")]
    [Route("api/Link")]
    public class LinkController : Controller
    {
        private static readonly ILog Logger = LogProvider.GetCurrentClassLogger();

        private readonly ILinkInfoEngine _linkInfoEngine;

        public LinkController(ILinkInfoEngine linkInfoEngine)
        {
            _linkInfoEngine = linkInfoEngine;
        }

        [HttpGet]
        [Route("ping")]
        public string Ping()
        {
            Logger.Info("Ping-Pong test");
            return "poing!";
        }

        //[HttpGet]
        //[Route("")]
        //// IEnumerable<LinkViewModel>
        //public IActionResult GetAllLinks()
        //{
        //    Logger.Info("GetAllLinks");
        //    var linkViewModels = _linkInfoEngine.GetAllLinks();
        //    return Ok(linkViewModels);
        //}

        //[HttpGet]
        //[Route("{linkId}")]
        //// LinkDetailViewModel
        //public IActionResult GetLinkDetail(int linkId)
        //{
        //    var linkDetailViewModel = _linkInfoEngine.GetLinkDetail(linkId);

        //    if (linkDetailViewModel == null)
        //    {
        //        return BadRequest();
        //    }

        //    return Ok(linkDetailViewModel);
        //}
    }
}