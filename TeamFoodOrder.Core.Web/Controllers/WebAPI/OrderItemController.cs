using System.Threading.Tasks;
using LibLog.Common.Logging;
using Microsoft.AspNetCore.Mvc;
using TeamFoodOrder.Domain;
using TeamFoodOrder.Engine;

namespace TeamFoodOrder.Core.Web.Controllers.WebAPI
{
    [Produces("application/json")]
    [Route("api/OrderItem")]
    public class OrderItemController : Controller
    {
        private static readonly ILog Logger = LogProvider.GetCurrentClassLogger();

        private readonly IFoodOrderEngine _foodOrderEngine;

        public OrderItemController(IFoodOrderEngine foodOrderEngine)
        {
            _foodOrderEngine = foodOrderEngine;
        }

        [HttpGet]
        [Route("")]
        public async Task<IActionResult> GetAll()
        {
            var all = await _foodOrderEngine.GetAllOrderItemsAsync();
            return Ok(all);
        }

        [HttpGet]
        [Route("{id}")]
        public async Task<IActionResult> GetSingle(int id)
        {
            var single = await _foodOrderEngine.GetOrderItemByIdAsync(id);
            if (single != null)
                return Ok(single);

            return NotFound();
        }

        [HttpPost]
        public async Task<OrderItem> PostAsync([FromBody] OrderItem orderItem)
        {
            return await _foodOrderEngine.InsertOrderItemAsync(orderItem);
        }

        [HttpPut]
        [Route("{id}")]
        public async Task<OrderItem> PutAsync(int id, [FromBody] OrderItem orderItem)
        {
            return await _foodOrderEngine.UpdateOrderItemAsync(id, orderItem);
        }

        [HttpDelete]
        [Route("{id}")]
        public async Task<OrderItem> DeleteAsync(int id)
        {
            return await _foodOrderEngine.DeoleteOrderItemAsync(id);
        }
    }
}