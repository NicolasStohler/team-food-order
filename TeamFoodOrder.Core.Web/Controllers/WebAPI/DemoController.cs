using LibLog.Common.Logging;
using Microsoft.AspNetCore.Mvc;

namespace TeamFoodOrder.Core.Web.Controllers.WebAPI
{
    [Produces("application/json")]
    [Route("api/Demo")]
    public class DemoController : Controller
    {
        private static readonly ILog Logger = LogProvider.GetCurrentClassLogger();

        [HttpGet]
        [Route("ping")]
        public IActionResult Ping()
        {
            return Ok("Hello pong");
        }
    }
}