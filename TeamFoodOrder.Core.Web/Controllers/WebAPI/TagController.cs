using LibLog.Common.Logging;
using Microsoft.AspNetCore.Mvc;
using TeamFoodOrder.Engine;

namespace TeamFoodOrder.Core.Web.Controllers.WebAPI
{
    [Produces("application/json")]
    [Route("api/Tag")]
    public class TagController : Controller
    {
        private static readonly ILog Logger = LogProvider.GetCurrentClassLogger();

        private readonly ILinkInfoEngine _linkInfoEngine;

        public TagController(ILinkInfoEngine linkInfoEngine)
        {
            _linkInfoEngine = linkInfoEngine;
        }

        //[HttpGet]
        //[Route("")]
        //// IEnumerable<TagViewModel>
        //public IActionResult GetAllTags()
        //{
        //    return Ok(_linkInfoEngine.GetAllTags());
        //}

        //[HttpGet]
        //[Route("{tagId:int}")]
        //// TagDetailViewModel
        //public IActionResult GetTagDetails(int tagId)
        //{
        //    return Ok(_linkInfoEngine.GetTagDetail(tagId));
        //}

        //[HttpGet]
        //[Route("{tagName}")]
        //// TagViewModel
        //public IActionResult GetTagByTagName(string tagName)
        //{
        //    var tag = _linkInfoEngine.GetTagByTagName(tagName);
        //    if (tag == null)
        //    {
        //        return NotFound();
        //    }
        //    return Ok(tag);
        //}

        //[HttpGet]
        //[Route("{tagName}/links")]
        //// IEnumerable<LinkViewModel>
        //public IActionResult GetLinksForTag(string tagName)
        //{
        //    var links = _linkInfoEngine.GetLinksForTag(tagName);
        //    if (links == null)
        //    {
        //        return NotFound();
        //    }
        //    return Ok(links);
        //}
    }
}