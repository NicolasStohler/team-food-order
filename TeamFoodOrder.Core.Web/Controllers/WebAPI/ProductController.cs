using System.Linq;
using System.Threading.Tasks;
using LibLog.Common.Logging;
using Microsoft.AspNetCore.Mvc;
using TeamFoodOrder.Engine;

namespace TeamFoodOrder.Core.Web.Controllers.WebAPI
{
    [Produces("application/json")]
    [Route("api/product")]
    public class ProductController : Controller
    {
        private static readonly ILog Logger = LogProvider.GetCurrentClassLogger();

        private readonly ILinkInfoEngine _linkInfoEngine;

        public ProductController(ILinkInfoEngine linkInfoEngine)
        {
            _linkInfoEngine = linkInfoEngine;
        }

        [HttpGet]
        [Route("names")]
        // IEnumerable<string>
        public async Task<IActionResult> GetAllNamesAsync()
        {
            var products = await _linkInfoEngine.GetAllProductsAsync();
            var names = products.Select(p => p.Name);
            return Ok(names);
        }

        [HttpGet]
        [Route("")]
        // IEnumerable<ProductViewModel>
        public async Task<IActionResult> GetAllProductsAsync()
        {
            var products = await _linkInfoEngine.GetAllProductsAsync();
            return Ok(products);
        }

        // [HttpGet("{productId}")] // route can also be put here
        [HttpGet]
        [Route("{productId:int}")]
        // ProductViewModel
        public async Task<IActionResult> GetProductAsync(int productId)
        {
            var product = await _linkInfoEngine.GetProductViewModelByProductIdAsync(productId);
            if (product == null)
            {
                return NotFound();  // 404 
            }
            return Ok(product);
        }

        [HttpGet]
        [Route("name/{name}")]
        // ProductViewModel
        public async Task<IActionResult> GetProductByNameAsync(string name)
        {
            var product = await _linkInfoEngine.GetProductViewModelAsync(name);
            if (product == null)
            {
                return NotFound();  // 404 
            }
            return Ok(product);
        }

        [HttpGet]
        [Route("name/{name}/{dbType}")]
        // ProductViewModel
        public async Task<IActionResult> GetProductByNameExtAsync(string name, string dbType)
        {
            var product = await _linkInfoEngine.GetProductViewModelAsync(name, dbType);
            if (product == null)
            {
                return NotFound();  // 404
            }
            return Ok(product);
        }

        [HttpGet]
        [Route("related/{name}")]
        // IEnumerable<ProductViewModel>
        public async Task<IActionResult> GetRelatedProductsAsync(string name)
        {
            var product = await _linkInfoEngine.GetRelatedProductViewModelsAsync(name);
            if (product == null)
            {
                return NotFound();  // 404
            }
            return Ok(product);
        }
    }
}