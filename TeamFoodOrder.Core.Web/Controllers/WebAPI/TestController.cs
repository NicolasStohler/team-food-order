using System;
using Microsoft.AspNetCore.Mvc;
using TeamFoodOrder.AppConfiguration;

namespace TeamFoodOrder.Core.Web.Controllers.WebAPI
{
    [Produces("application/json")]
    [Route("api/test")]
    public class TestController : Controller
    {
        private readonly IMyTestType _myTestType;
        private readonly IAppConfig _appConfig;

        public TestController(IMyTestType myTestType, IAppConfig appConfig)
        {
            _myTestType = myTestType;
            _appConfig = appConfig;
        }

        [HttpGet]
        [Route("")]
        // string
        public IActionResult Get()
        {
            return Ok(new
            {
                Message = $"build {_appConfig.MySetting}",
                Dev = _myTestType.GetDevName(),
                Date = DateTime.Now,
            });
        }

        [HttpGet]
        [Route("person")]
        // Person
        public IActionResult GetPerson()
        {
            return Ok(
                new Person
                {
                    Name = "Stohler",
                    Vorname = "Nicolas",
                    Year = 1975
                }
            );
        }
    }

    public class Person
    {
        public string Name { get; set; }
        public string Vorname { get; set; }
        public int Year { get; set; }
    }
}