﻿using System;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using LibLog.Common.Logging;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Serilog;

namespace TeamFoodOrder.Core.Web
{
    public class Startup
    {
        private ILog _logger;

        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }
        public IContainer ApplicationContainer { get; private set; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            InitializeLogger();

            services.AddCors();
            // Add framework services.
            services.AddMvc();

            // http://docs.autofac.org/en/latest/integration/aspnetcore.html
            // Create the container builder.

            // Register dependencies, populate the services from
            // the collection, and build the container. If you want
            // to dispose of the container at the end of the app,
            // be sure to keep a reference to it as a property or field.

            var autofacContainer = Bootstrapper.Bootstrapper.Initialize(builder =>
            {
                builder.RegisterType<MyTestType>().As<IMyTestType>();

                builder.Populate(services);
            });

            this.ApplicationContainer = autofacContainer;

            // Create the IServiceProvider based on the container.
            return new AutofacServiceProvider(this.ApplicationContainer);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseCors(b => b
                .AllowCredentials()
                .AllowAnyOrigin()
                //.WithOrigins()
                //.WithOrigins("http://localhost:4200")
                //.WithOrigins("http://localhost")
                //.WithOrigins("http://e200681-web01")
                //.WithOrigins("http://lampen.apptest.local.dom")
                //.WithOrigins("http://e200681-web01/lampen-tableau-service")
                ////.WithOrigins("http://localhost/lampen-tableau")
                .AllowAnyHeader()
                .AllowAnyMethod()
                // .WithMethods("GET")
                );

            app.UseStaticFiles();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }

        private void InitializeLogger()
        {
            var baseDir = AppDomain.CurrentDomain.BaseDirectory;
            var logFileFormat = System.IO.Path.Combine(baseDir, "logs", "TeamFoodOrder.Web-{Date}.txt");

            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Verbose()
                .WriteTo.LiterateConsole(restrictedToMinimumLevel: Serilog.Events.LogEventLevel.Debug)
                .WriteTo.RollingFile(logFileFormat, restrictedToMinimumLevel: Serilog.Events.LogEventLevel.Verbose)
                .CreateLogger();

            Log.Information("App Startup (Serilog Log.Information)");

            _logger = LogProvider.GetCurrentClassLogger();
            _logger.Info("Startup (via LibLog interface)");
        }
    }
    
}
