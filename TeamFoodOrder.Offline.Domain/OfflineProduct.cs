﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConnString.Domain;
using ConnString.ViewModels;
using Force.Crc32;
using EnsureThat;

namespace ConnString.Offline.Domain
{
    public class OfflineProduct
    {
        [Key]
        public int OfflineProductId { get; set; }
        public int ProductId { get; set; }
        public string Name { get; set; }                // "iasibe2-formular-frm1234": fallback to "iasibe2-formular", then "iasibe2"
        // with option of no-fallback!
        public string ConnectionString { get; set; }
        public DateTime ValidFrom { get; set; }
        public DateTime? ValidTo { get; set; }
        public string DbType { get; set; }              // string or foreign key to a dbType table? default ("production")
        public string Crc32 { get; set; }
        public DateTime OfflineValidUntil { get; set; }

        public string GetCalculatedCrc32()
        {
            var propertyNames = Product.Crc32PropertyNames;

            var sb = new StringBuilder();
            var properties = this.GetType()
                .GetProperties()
                .Where(p => propertyNames.Contains(p.Name))
                .ToList();

            foreach (var propertyInfo in properties)
            {
                var value = propertyInfo.GetValue(this, null);
                var valueAsString = value?.ToString();
                sb.Append($"{propertyInfo.Name}:{valueAsString}|");
            }
            var bytes = Encoding.ASCII.GetBytes(sb.ToString());
            var crc32 = Crc32Algorithm.Compute(bytes);
            var crc32Text = crc32.ToString("x8");
            return crc32Text;
        }
    }
}
