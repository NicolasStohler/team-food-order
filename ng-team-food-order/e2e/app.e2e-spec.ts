import { NgTeamFoodOrderPage } from './app.po';

describe('ng-team-food-order App', () => {
  let page: NgTeamFoodOrderPage;

  beforeEach(() => {
    page = new NgTeamFoodOrderPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
