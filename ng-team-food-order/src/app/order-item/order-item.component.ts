import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {OrderItem} from '../order-item';
import {OrderItemService} from '../services/order-item.service';
import * as _ from 'lodash';

@Component({
  selector: 'app-order-item',
  templateUrl: './order-item.component.html',
  styleUrls: ['./order-item.component.css']
})
export class OrderItemComponentComponent implements OnInit {

  @Input()
  orderItem: OrderItem;

  @Output()
  deleted: EventEmitter<OrderItem> = new EventEmitter();

  editOrderItem: OrderItem;
  isProcessing = false;
  isAccordionOpen = false;

  constructor(
    private orderItemService: OrderItemService
  ) { }

  ngOnInit() {
    this.editOrderItem = _.clone(this.orderItem);
  }

  cancelEdit() {
    this.editOrderItem = _.clone(this.orderItem);
    // this.isAccordionOpen = true;
    // this.isAccordionOpen = false;
  }

  update() {
    this.isProcessing = true;
    this.editOrderItem.lastModified = new Date();
    const clone = _.clone(this.editOrderItem);
    clone.orderItemId = null;         // for firebase: prevent creation of 'key' entries on items

    this.orderItemService.updateOrderItem(this.orderItem.orderItemId, clone)
      .subscribe(data => {
        console.log('updated:', data);
        this.orderItem = _.clone(this.editOrderItem);
        this.isProcessing = false;
        this.isAccordionOpen = false;
      });
  }

  delete() {
    this.isProcessing = true;
    this.orderItemService.deleteOrderItem(this.orderItem.orderItemId)
      .subscribe(data => {
        console.log('deleted:', data);
        // http://stackoverflow.com/a/35941067/54159
        this.deleted.emit(this.orderItem);
        this.isProcessing = false;
        this.isAccordionOpen = false;
      });
  }

}
