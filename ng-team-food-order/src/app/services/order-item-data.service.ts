import { Injectable } from '@angular/core';
import * as _ from 'lodash';  // http://stackoverflow.com/a/40689619/54159

import {EditOrderItem} from '../order-view/edit-order-item';
import {OrderItem} from '../order-item';
import {mapObjectToClassInstance} from '../mapObjectToClassInstance';
import {OrderItemService} from './order-item.service';

@Injectable()
export class OrderItemDataService {

  editOrderItems: Array<EditOrderItem> = [];

  constructor(
    private orderItemService: OrderItemService
  ) { }

  add(addOrderItem: OrderItem): EditOrderItem {
    const eo = new EditOrderItem(addOrderItem, _.clone(addOrderItem), false);
    this.editOrderItems.push(eo);
    return eo;
  }
}
