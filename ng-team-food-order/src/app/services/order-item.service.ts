import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import {Observable} from 'rxjs/Rx';

// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import {OrderItem} from '../order-item';

@Injectable()
export class OrderItemService {
  // class adapted from https://scotch.io/tutorials/angular-2-http-requests-with-observables
  // private orderItemsUrl = 'http://localhost:12850/api/orderitem';
  private orderItemsUrl = 'https://teamfoodorder.firebaseio.com/orderItem';
  private urlPostFix = '.json';   // for firebase

  constructor(private http: Http) { }

  // Fetch all existing items
  getList(): Observable<OrderItem[]> {
    // ...using get request
    return this.http.get(`${this.orderItemsUrl}${this.urlPostFix}`)
      // ...and calling .json() on the response to return data
      .map((res: Response) => res.json())
      // ...errors if any
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  // Fetch one item by id
  getById(id: string): Observable<OrderItem> {
    // ...using get request
    return this.http.get(`${this.orderItemsUrl}/${id}${this.urlPostFix}`)
      // ...and calling .json() on the response to return data
      .map((res: Response) => res.json())
      // ...errors if any
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  // add a new item
  addOrderItem(orderItem: OrderItem): Observable<OrderItem> {
    // const bodyString = JSON.stringify(orderItem); // Stringify payload // stn: not required anymore
    const headers    = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
    const options    = new RequestOptions({ headers: headers }); // Create a request option

    return this.http.post(`${this.orderItemsUrl}${this.urlPostFix}`, orderItem, options) // ...using orderItem request
      .map((res: Response) => res.json()) // ...and calling .json() on the response to return data
      .catch((error: any) => Observable.throw(error.json().error || 'Server error')); // ...errors if any
  }

  // update an item
  updateOrderItem(id: string, orderItem: OrderItem): Observable<OrderItem[]> {
    console.log('update item', orderItem);
    // const bodyString = JSON.stringify(body); // Stringify payload
    const headers = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
    const options = new RequestOptions({ headers: headers }); // Create a request option

    return this.http.put(`${this.orderItemsUrl}/${id}${this.urlPostFix}`, orderItem, options) // ...using put request
      .map((res: Response) => res.json()) // ...and calling .json() on the response to return data
      .catch((error: any) => Observable.throw(error.json().error || 'Server error')); // ...errors if any
  }

  // delete an item
  deleteOrderItem(id: string): Observable<OrderItem[]> {
    return this.http.delete(`${this.orderItemsUrl}/${id}${this.urlPostFix}`) // ...using put request
      .map((res: Response) => res.json()) // ...and calling .json() on the response to return data
      .catch((error: any) => Observable.throw(error.json().error || 'Server error')); // ...errors if any
  }
}
