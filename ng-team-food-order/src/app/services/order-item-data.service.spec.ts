import { TestBed, inject } from '@angular/core/testing';

import { OrderItemDataService } from './order-item-data.service';

describe('OrderItemDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [OrderItemDataService]
    });
  });

  it('should ...', inject([OrderItemDataService], (service: OrderItemDataService) => {
    expect(service).toBeTruthy();
  }));
});
