import {Directive, HostListener} from '@angular/core';

// http://stackoverflow.com/a/41001184/54159

@Directive({
  selector: '[appClickStopPropagation]'
})
export class ClickStopPropagationDirective {
  @HostListener('click', ['$event'])
  public onClick(event: any): void
  {
    event.stopPropagation();
  }
}
