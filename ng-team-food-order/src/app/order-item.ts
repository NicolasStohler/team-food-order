export class OrderItem {
  userName: string;
  foodName: string;
  comment: string;
  lastModified: Date;

  public key: string;

  get orderItemId(): string {
    return this.key;
  }

  set orderItemId(value: string) {
    this.key = value;
  }

}
