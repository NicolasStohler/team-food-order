import {OrderItem} from '../order-item';

export class EditOrderItem {
  isProcessing = false;

  constructor (
    public orderItem: OrderItem,
    public editItem: OrderItem,
    public isEditorOpen: boolean
  ) {}
}
