import {Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators, ValidatorFn, AbstractControl } from '@angular/forms';
import * as _ from 'lodash';

import {OrderItem} from '../order-item';
import {OrderItemService} from '../services/order-item.service';  // http://stackoverflow.com/a/40689619/54159
import {mapObjectToClassInstance} from 'app/mapObjectToClassInstance';
import {EditOrderItem} from './edit-order-item';
import {OrderItemDataService} from '../services/order-item-data.service';

@Component({
  selector: 'app-order-view',
  templateUrl: './order-view.component.html',
  styleUrls: ['./order-view.component.css']
})
export class OrderViewComponent implements OnInit {

  editOrderItems: Array<EditOrderItem> = [];
  openOneAtATime = true;

  constructor(
    private orderItemService: OrderItemService,
    private orderItemDataService: OrderItemDataService
  ) { }

  ngOnInit() {
    this.editOrderItems = this.orderItemDataService.editOrderItems;
    this.load();
  }

  private load() {
    this.orderItemService.getList()
      .subscribe(
        data => {
          if (data !== null) {
            const orderItems = mapObjectToClassInstance(data).map(x => Object.assign(new OrderItem(), x));
            console.log('loaded xs', orderItems);
            // this.editOrderItems = [];
            this.orderItemDataService.editOrderItems.length = 0;
            orderItems.forEach(o => {
              const editOrderItem = this.orderItemDataService.add(o);
            });
            console.log('loaded', this.editOrderItems);
          }
        });
  }

  private getActiveEditOrderItem() {
    const item = _.find(this.editOrderItems, x => x.isEditorOpen === true);
    return item;
  }

  cancelFromEditor(editItem) {
    const activeItem = this.getActiveEditOrderItem();
    this.cancelEdit(activeItem);
  }

  updateFromEditor(editItem) {
    console.log('updateFromEditor', editItem);
    const activeItem = this.getActiveEditOrderItem();
    activeItem.editItem = editItem;
    this.update(activeItem);
  }

  deleteFromEditor(editItem) {
    const activeItem = this.getActiveEditOrderItem();
    this.delete(activeItem);
  }

  update(editOrderItem: EditOrderItem) {
    editOrderItem.isProcessing = true;
    editOrderItem.editItem.lastModified = new Date();
    const clone = _.clone(editOrderItem.editItem);
    clone.orderItemId = null;         // for firebase: prevent creation of 'key' entries on items

    this.orderItemService.updateOrderItem(editOrderItem.orderItem.orderItemId, clone)
      .subscribe(data => {
        console.log('updated:', data);
        editOrderItem.orderItem = _.clone(editOrderItem.editItem);
        editOrderItem.isProcessing = false;
        this.closeEditor(editOrderItem);
      });
  }

  delete(editOrderItem: EditOrderItem) {
    editOrderItem.isProcessing = true;
    console.log('deleting: ', editOrderItem);
    this.orderItemService.deleteOrderItem(editOrderItem.orderItem.orderItemId)
      .subscribe(data => {
        console.log('deleted:', data);
        // http://stackoverflow.com/a/35941067/54159
        _.remove(this.editOrderItems, o => o.orderItem.orderItemId === editOrderItem.orderItem.orderItemId);
        editOrderItem.isProcessing = false;
        this.closeEditor(editOrderItem);
      });
  }

  cancelEdit(editOrderItem: EditOrderItem) {
    editOrderItem.editItem = _.clone(editOrderItem.orderItem);
    this.closeEditor(editOrderItem);
  }

  test() {
    this.editOrderItems[0].isEditorOpen = !this.editOrderItems[0].isEditorOpen;
  }

  clickAccGrp(editOrderItem: EditOrderItem) {
    editOrderItem.isEditorOpen = !editOrderItem.isEditorOpen;
    // ensure only one isEditorOpen flag is true, others all false!
    this.editOrderItems.forEach(e => {
      if (e.orderItem.orderItemId !== editOrderItem.orderItem.orderItemId) {
        e.isEditorOpen = false;
      }
    });
  }

  closeEditor(editOrderItem: EditOrderItem) {
    editOrderItem.isEditorOpen = false;
  }

}
