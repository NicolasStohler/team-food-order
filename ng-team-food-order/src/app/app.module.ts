import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { ReactiveFormsModule } from '@angular/forms';
import { Ng2BootstrapModule, AccordionModule } from 'ngx-bootstrap';

import { AppComponent } from './app.component';
import { OrderItemComponentComponent } from './order-item/order-item.component';
import {OrderItemService} from './services/order-item.service';
import { OrderViewComponent } from './order-view/order-view.component';
import { ClickStopPropagationDirective } from './directives/click-stop-propagation.directive';
import {OrderItemDataService} from './services/order-item-data.service';
import { OrderEditorComponent } from './order-editor/order-editor.component';

@NgModule({
  declarations: [
    AppComponent,
    OrderItemComponentComponent,
    OrderViewComponent,
    ClickStopPropagationDirective,
    OrderEditorComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    ReactiveFormsModule,
    Ng2BootstrapModule.forRoot(),
    AccordionModule.forRoot()
  ],
  providers: [
    OrderItemService,
    OrderItemDataService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
