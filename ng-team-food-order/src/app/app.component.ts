import { Component, OnInit, OnChanges } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators, ValidatorFn, AbstractControl } from '@angular/forms';

import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import {OrderItem} from './order-item';
import {OrderItemService} from './services/order-item.service';
import {OrderItemDataService} from './services/order-item-data.service';
import {EditOrderItem} from './order-view/edit-order-item';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnChanges {

  title = 'team-food-order app';
  // addOrderItem: OrderItem = new OrderItem();
  editOrderItems: Array<EditOrderItem> = [];
  addForm: FormGroup;
  nameChangeLog: string[] = [];

  formErrors = {
    'userName': '',
    'foodName': ''
  };

  validationMessages = {
    'userName': {
      'required':      'Name is required.',
      'minlength':     'Name must be at least 3 characters long.',
      'forbiddenName': 'Someone named "Bob" cannot be a hero.'
    },
    'foodName': {
      'required': 'Dish is required.'
    }
  };

  constructor(private orderItemService: OrderItemService,
              private orderItemDataService: OrderItemDataService,
              private fb: FormBuilder) {

    this.createForm();
    this.logNameChange();
  }

  ngOnInit() {

    this.editOrderItems = this.orderItemDataService.editOrderItems;
  }

  createForm() {
    this.addForm = this.fb.group({
      userName: ['', [
          Validators.required,
          Validators.minLength(3),
          this.forbiddenNameValidator(/bob/i)
        ]
      ],
      foodName: ['', Validators.required]
    });

    this.addForm.valueChanges
      .subscribe(data => this.onValueChanged(data));

    this.onValueChanged(); // (re)set validation messages now
  }

  /** A hero's name can't match the given regular expression */
  forbiddenNameValidator(nameRe: RegExp): ValidatorFn {
    return (control: AbstractControl): {[key: string]: any} => {
      const name = control.value;
      const no = nameRe.test(name);
      return no ? {'forbiddenName': {name}} : null;
    };
  }

  // addNewOrderItem() {
  //   this.addOrderItem.lastModified = new Date();
  //   this.orderItemService.addOrderItem(this.addOrderItem)
  //     .subscribe(
  //       data => {
  //         // firebase: data.name is newly generated key
  //         console.log('addNewOrderItem data is ', data);
  //         this.addOrderItem.orderItemId = data['name'];
  //         this.orderItemDataService.add(this.addOrderItem);
  //         this.addOrderItem = new OrderItem();
  //       });
  // }

  // ---
  // createForm() {
  //   this.heroForm = this.fb.group({
  //     name: '',
  //     secretLairs: this.fb.array([]),
  //     power: '',
  //     sidekick: ''
  //   });
  // }

  ngOnChanges() {
    this.addForm.reset({
      // userName: this.addOrderItem.userName,
      // foodName: this.
    });
    // this.setAddresses(this.hero.addresses);
  }

  onSubmit() {
    const addOrderItem = this.prepareSaveOrderItem();
    // this.heroService.updateHero(this.hero).subscribe(/* error handling */);

    this.orderItemService.addOrderItem(addOrderItem)
      .subscribe(
        data => {
          // firebase: data.name is newly generated key
          console.log('addNewOrderItem data is ', data);
          // this.addOrderItem.orderItemId = data['name'];
          this.orderItemDataService.add(addOrderItem);
          // this.addOrderItem = new OrderItem();
        });

    this.ngOnChanges();
  }

  prepareSaveOrderItem(): OrderItem {
    const formModel = this.addForm.value;

    // // deep copy of form model lairs
    // const secretLairsDeepCopy: Address[] = formModel.secretLairs.map(
    //   (address: Address) => Object.assign({}, address)
    // );

    // return new `Hero` object containing a combination of original hero value(s)
    // and deep copies of changed form model values
    const saveHero: OrderItem = {
      userName: formModel.userName as string,
      foodName: formModel.foodName as string,
      comment: '',
      lastModified: new Date(),
      key: null,
      orderItemId: '',
      // addresses: formModel.secretLairs // <-- bad!
      // addresses: secretLairsDeepCopy
    };
    return saveHero;
  }

  revert() {
    this.ngOnChanges();
  }

  logNameChange() {
    // console.log('logNameChange');
    const nameControl = this.addForm.get('userName');
    nameControl.valueChanges.forEach(
      (value: string) => this.nameChangeLog.push(value)
    );
  }

  // https://angular.io/docs/ts/latest/cookbook/form-validation.html#!#live-example
  onValueChanged(data?: any) {
    if (!this.addForm) { return; }
    const form = this.addForm;

    for (const field in this.formErrors) {
      if (this.formErrors.hasOwnProperty(field)) {
        // clear previous error message (if any)
        this.formErrors[field] = '';
        const control = form.get(field);

        if (control && control.dirty && !control.valid) {
          const messages = this.validationMessages[field];
          for (const key in control.errors) {
            if (control.errors.hasOwnProperty(key)) {
              this.formErrors[field] += messages[key] + ' ';
            }
          }
        }
      }
    }
  }


}
