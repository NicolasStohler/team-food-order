import {Component, Input, OnInit, OnChanges, Output, EventEmitter} from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators, ValidatorFn, AbstractControl } from '@angular/forms';
import * as _ from 'lodash';

import {OrderItem} from '../order-item';
import {OrderItemService} from "../services/order-item.service";

@Component({
  selector: 'app-order-editor',
  templateUrl: './order-editor.component.html',
  styleUrls: ['./order-editor.component.css']
})
export class OrderEditorComponent implements OnInit, OnChanges {

  @Input()
  orderItem: OrderItem;

  @Output() update: EventEmitter<OrderItem> = new EventEmitter();
  @Output() cancel: EventEmitter<OrderItem> = new EventEmitter();
  @Output() delete: EventEmitter<OrderItem> = new EventEmitter();

  editForm: FormGroup;

  formErrors = {
    'userName': '',
    'foodName': ''
  };

  validationMessages = {
    'userName': {
      'required':      'Name is required.',
      'minlength':     'Name must be at least 3 characters long.',
      'forbiddenName': 'Someone named "Bob" cannot be a hero.'
    },
    'foodName': {
      'required': 'Dish is required.'
    }
  };

  constructor(
    private orderItemService: OrderItemService,
    private fb: FormBuilder
  ) {
    this.createForm();
  }

  ngOnInit() {
  }

  ngOnChanges(): void {
    this.editForm.reset({
      userName: this.orderItem.userName,
      foodName: this.orderItem.foodName
    });
    // this.setAddresses(this.hero.addresses);
  }

  createForm() {
    this.editForm = this.fb.group({
      userName: ['', [
        Validators.required,
        Validators.minLength(3)
        // this.forbiddenNameValidator(/bob/i)
      ]
      ],
      foodName: ['', Validators.required]
    });

    this.editForm.valueChanges
      .subscribe(data => this.onValueChanged(data));

    this.onValueChanged(); // (re)set validation messages now
  }


  onSubmit() {
    const editItem = this.prepareSaveOrderItem();
    // this.heroService.updateHero(this.hero).subscribe(/* error handling */);

    // this.orderItemService.addOrderItem(addOrderItem)
    //   .subscribe(
    //     data => {
    //       // firebase: data.name is newly generated key
    //       console.log('addNewOrderItem data is ', data);
    //       // this.addOrderItem.orderItemId = data['name'];
    //       this.orderItemDataService.add(addOrderItem);
    //       // this.addOrderItem = new OrderItem();
    //     });
    console.log('submit', this.orderItem.orderItemId, editItem);
    this.update.emit(editItem);

    // this.orderItemService.updateOrderItem(this.orderItem.orderItemId, editItem)
    //   .subscribe(data => {
    //     console.log('updated:', data);
    //     // editOrderItem.orderItem = _.clone(editOrderItem.editItem);
    //     // editOrderItem.isProcessing = false;
    //     // this.closeEditor(editOrderItem);
    //   });

    this.ngOnChanges();
  }

  prepareSaveOrderItem(): OrderItem {
    const formModel = this.editForm.value;

    // // deep copy of form model lairs
    // const secretLairsDeepCopy: Address[] = formModel.secretLairs.map(
    //   (address: Address) => Object.assign({}, address)
    // );

    // return new `Hero` object containing a combination of original hero value(s)
    // and deep copies of changed form model values
    // const saveHero: OrderItem = {
    //   userName: formModel.userName as string,
    //   foodName: formModel.foodName as string,
    //   comment: '',
    //   lastModified: new Date(),
    //   key: '',
    //   orderItemId: '',
    //   // addresses: formModel.secretLairs // <-- bad!
    //   // addresses: secretLairsDeepCopy
    // };
    const saveItem = _.clone(this.orderItem);
    saveItem.foodName = formModel.foodName;
    saveItem.lastModified = new Date();

    return saveItem;
  }

  revert() {
    this.ngOnChanges();
    this.cancel.emit(this.orderItem);
  }

  // https://angular.io/docs/ts/latest/cookbook/form-validation.html#!#live-example
  onValueChanged(data?: any) {
    if (!this.editForm) { return; }
    const form = this.editForm;

    for (const field in this.formErrors) {
      if (this.formErrors.hasOwnProperty(field)) {
        // clear previous error message (if any)
        this.formErrors[field] = '';
        const control = form.get(field);

        if (control && control.dirty && !control.valid) {
          const messages = this.validationMessages[field];
          for (const key in control.errors) {
            if (control.errors.hasOwnProperty(key)) {
              this.formErrors[field] += messages[key] + ' ';
            }
          }
        }
      }
    }
  }

  deleteItem() {
    this.delete.emit(this.orderItem);
  }

}
