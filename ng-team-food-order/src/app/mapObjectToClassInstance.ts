
// intended for firebase data, but should work for normal json data from any web api
// http://stackoverflow.com/a/41608763/54159
export function mapObjectToClassInstance(object: Object) {
  return Object.keys(object).map((key) => {
    return Object.assign(object[key], {key});
  });
}
