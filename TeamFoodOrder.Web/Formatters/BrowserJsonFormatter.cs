﻿using System;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using Newtonsoft.Json;

namespace ConnString.Web.Formatters
{
	// http://stackoverflow.com/questions/9847564/how-do-i-get-asp-net-web-api-to-return-json-instead-of-xml-using-chrome
	public class BrowserJsonFormatter : JsonMediaTypeFormatter
	{
		public BrowserJsonFormatter()
		{
			this.SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/html"));
			this.SerializerSettings.Formatting = Formatting.Indented;

			// to prevent self referencing loop json exception (Newtonsoft.Json.JsonSerializationException):
			// http://stackoverflow.com/a/19664589/54159
			this.SerializerSettings.PreserveReferencesHandling = PreserveReferencesHandling.Objects;
			this.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
		}

		public override void SetDefaultContentHeaders(Type type, HttpContentHeaders headers, MediaTypeHeaderValue mediaType)
		{
			base.SetDefaultContentHeaders(type, headers, mediaType);
			headers.ContentType = new MediaTypeHeaderValue("application/json") {CharSet = "utf-8"};
		}
	}
}
