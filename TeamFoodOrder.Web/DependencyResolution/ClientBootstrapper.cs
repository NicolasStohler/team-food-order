﻿using System;
using Autofac;
using ConnString.Bootstrapper;

namespace ConnString.Web.DependencyResolution
{
	public static class ClientBootstrapper
	{
		public static IContainer AutofacContainer = null;

		public static void Initialize()
		{
			Initialize(null);
		}

		public static void Initialize(Action<ContainerBuilder> containerBuilder)
		{
			AutofacContainer = AutofacLoader.Initialize(containerBuilder);
		}
	}
}
