﻿using System.Reflection;
using System.Web.Http;
using Autofac;
using Autofac.Integration.WebApi;

namespace ConnString.Web.DependencyResolution
{
	public static class ClientBootstrapperWebExtender
	{
		public static void Extend(ContainerBuilder builder)
		{
			//builder.RegisterAssemblyTypes(typeof(MvcApplication).Assembly)
			//    .Where(t => t.Name.EndsWith("Adapter"))
			//    .As(t => t.GetInterfaces().FirstOrDefault(i => i.Name == "I" + t.Name));

			//builder.RegisterControllers(typeof(MvcApplication).Assembly);

			builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

			var config = GlobalConfiguration.Configuration;

			builder.RegisterHttpRequestMessage(config);
		}
	}
}
