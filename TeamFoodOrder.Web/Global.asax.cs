﻿using System;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Autofac.Integration.WebApi;
using ConnString.Bootstrapper;
using ConnString.Web.DependencyResolution;
using LibLog.Common.Logging;
using Serilog;

namespace ConnString.Web
{
	public class MvcApplication : System.Web.HttpApplication
	{
		private ILog _logger;

		protected void Application_Start()
		{
			InitializeLogger();

			// possible to refctor boostrapper stuff?
			ClientBootstrapper.Initialize(builder =>
			{
				AutoMapperConfig.RegisterForDI(builder);
				ClientBootstrapperWebExtender.Extend(builder);
			});

			var container = ClientBootstrapper.AutofacContainer;

			GlobalConfiguration.Configuration.DependencyResolver = new AutofacWebApiDependencyResolver(container); // web api controllers

			AreaRegistration.RegisterAllAreas();
			GlobalConfiguration.Configure(WebApiConfig.Register);
			FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
			RouteConfig.RegisterRoutes(RouteTable.Routes);
			BundleConfig.RegisterBundles(BundleTable.Bundles);

			Log.Information("Application_Start exit");
		}

		private void InitializeLogger()
		{
			var baseDir = AppDomain.CurrentDomain.BaseDirectory;
			var logFileFormat = System.IO.Path.Combine(baseDir, "logs", "ConnString.Web-{Date}.txt");

			Log.Logger = new LoggerConfiguration()
							.MinimumLevel.Verbose()
							.WriteTo.LiterateConsole(restrictedToMinimumLevel: Serilog.Events.LogEventLevel.Debug)
							.WriteTo.RollingFile(logFileFormat, restrictedToMinimumLevel: Serilog.Events.LogEventLevel.Verbose)
							.CreateLogger();

			Log.Information("App Startup (Serilog Log.Information)");

			_logger = LogProvider.GetCurrentClassLogger();
			_logger.Info("Startup (via LibLog interface)");
		}
	}
}
