﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using ConnString.Engine;
using ConnString.ViewModels;
using LibLog.Common.Logging;

namespace ConnString.Web.Controllers.WebApi
{
	[RoutePrefix("api/product")]
	public class ProductController : ApiController
	{
		private static readonly ILog Logger = LogProvider.GetCurrentClassLogger();

		private readonly ILinkInfoEngine _linkInfoEngine;

		public ProductController(ILinkInfoEngine linkInfoEngine)
		{
			_linkInfoEngine = linkInfoEngine;
		}

		[HttpGet]
		[Route("")]
		public async Task<IEnumerable<ProductViewModel>> GetAllProductsAsync()
		{
			// Logger.Info("GetAllProducts");
			var products = await _linkInfoEngine.GetAllProductsAsync();
			return products;
		}

		//[HttpGet]
		//[Route("{productId}")]
		//public ProductDetailViewModel GetProductDetailWithLinks(int productId)
		//{
		//	var product = _linkInfoEngine.GetProductDetail(productId);
		//	// Logger.InfoFormat("Product: {@product}", product);
		//	return product;
		//}
	}
}
