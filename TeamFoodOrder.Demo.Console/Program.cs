﻿using System;
using System.Threading.Tasks;
using Autofac;
using EnsureThat;
using LibLog.Common.Logging;
using Serilog;
using TeamFoodOrder.Engine;

namespace TeamFoodOrder.Demo.Console
{
    internal class Program
    {
        private static ILog _logger;

        private static void Main(string[] args)
        {
            RunAsync().Wait();
        }

        static async Task RunAsync()
        {
            InitializeLogger();

            var autofacContainer = Bootstrapper.Bootstrapper.Initialize();

            try
            {
                using (var scope = autofacContainer.BeginLifetimeScope())
                {
                    IFoodOrderEngine foodOrderEngine = scope.Resolve<IFoodOrderEngine>();
                    Ensure.That(foodOrderEngine, "foodOrderEngine").IsNotNull();

                    var orderItems = await foodOrderEngine.GetAllOrderItemsAsync();

                    foreach (var orderItem in orderItems)
                    {
                        _logger.InfoFormat("orderItem: {name} by {username}", orderItem.FoodName, orderItem.UserName);
                    }

                    orderItems = await foodOrderEngine.GetOrderItemsByUserNameAsync("David");

                    foreach (var orderItem in orderItems)
                    {
                        _logger.InfoFormat("orderItem: {name} by {username}", orderItem.FoodName, orderItem.UserName);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.ErrorException("Error", ex);
                throw;
            }

            System.Console.WriteLine("done");
            System.Console.ReadLine();
        }

        private static void InitializeLogger()
        {
            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Verbose()
                .WriteTo.LiterateConsole(restrictedToMinimumLevel: Serilog.Events.LogEventLevel.Debug)
                .WriteTo.RollingFile("logs\\TeamFoodOrder-{Date}.txt",
                    restrictedToMinimumLevel: Serilog.Events.LogEventLevel.Verbose)
                .CreateLogger();

            Log.Information("App Startup (Serilog Log.Information)");

            _logger = LogProvider.GetCurrentClassLogger();
            _logger.Info("Startup (via LibLog interface)");
        }
    }
}