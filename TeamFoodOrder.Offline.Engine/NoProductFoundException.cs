﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ConnString.Offline.Engine
{
    public class NoProductFoundException : Exception
    {
        public NoProductFoundException()
        {
        }

        public NoProductFoundException(string message) : base(message)
        {
        }

        public NoProductFoundException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected NoProductFoundException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
