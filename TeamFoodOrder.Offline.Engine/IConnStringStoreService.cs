﻿using System.Threading.Tasks;

namespace ConnString.Offline.Engine
{
    public interface IConnStringStoreService
    {
        Task<string> GetConnectionStringAsync();
    }
}