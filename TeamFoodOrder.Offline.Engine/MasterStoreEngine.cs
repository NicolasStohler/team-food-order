﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using ConnString.AppConfiguration;
using ConnString.Domain;
using ConnString.ViewModels;
using LibLog.Common.Logging;

namespace ConnString.Offline.Engine
{
    internal class MasterStoreEngine : IMasterStoreEngine
    {
        // https://docs.microsoft.com/en-us/aspnet/web-api/overview/advanced/calling-a-web-api-from-a-net-client

        private static readonly ILog Logger = LogProvider.GetCurrentClassLogger();

        private readonly HttpClient _client = new HttpClient();
        private readonly IConnectionStringStoreConfig _connectionStringStoreConfig;

        public MasterStoreEngine(IConnectionStringStoreConfig connectionStringStoreConfig)
        {
            _connectionStringStoreConfig = connectionStringStoreConfig;

            // _client.Timeout  = TimeSpan.FromMilliseconds(1);          // for testing timeout exception (TaskCanceledException)

            _client.Timeout     = TimeSpan.FromSeconds(_connectionStringStoreConfig.HttpClientTimeoutSeconds);
            _client.BaseAddress = new Uri(_connectionStringStoreConfig.HttpClientBaseAddress); 
            _client.DefaultRequestHeaders.Accept.Clear();
            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public async Task<IEnumerable<string>> GetAllNamesAsync()
        {
            const string path = "api/product/names";
            var response = await _client.GetAsync(path);
            if (response.IsSuccessStatusCode)
            {
                var result = await response.Content.ReadAsAsync<IEnumerable<string>>();
                return result;
            }
            return new List<string>();
        }

        public async Task<IEnumerable<ProductViewModel>> GetAllProductsAsync()
        {
            var path = "api/product";
            var products = await GetProductViewModelsFromPathAsync(path);
            return products;
        }

        public async Task<ProductViewModel> GetProductByProductIdAsync(int productId)
        {
            var path = $"api/product/{productId}";
            var result = await GetProductViewModelFromPathAsync(path);
            return result;
        }

        public async Task<ProductViewModel> GetProductByNameAsync(string name)
        {
            var path = $"api/product/name/{name}";
            var result = await GetProductViewModelFromPathAsync(path);
            return result;
        }

        public async Task<ProductViewModel> GetProductViewModelAsync(string name, string dbType)
        {
            var sw = Stopwatch.StartNew();
            ProductViewModel product = null;
            try
            {
                var path = $"api/product/name/{name}/{dbType}";
                product = await GetProductViewModelFromPathAsync(path);
                
            }
            catch (Exception ex)
            {
                Logger.ErrorException("Exception", ex);
            }
            sw.Stop();
            Logger.InfoFormat("MasterStoreEngine:GetProductViewModelAsync elapsed = {sw}", sw.Elapsed);
            return product;
        }

        public async Task<IEnumerable<ProductViewModel>> GetRelatedProductsViewModelsAsync(string name)
        {
            var sw = Stopwatch.StartNew();
            IEnumerable<ProductViewModel> products = new List<ProductViewModel>();
            try
            {
                var path = $"api/product/related/{name}";
                products = await GetProductViewModelsFromPathAsync(path);
            }
            catch (Exception ex)
            {
                Logger.ErrorException("Exception", ex);
            }
            sw.Stop();
            Logger.InfoFormat("MasterStoreEngine:GetRelatedProductsViewModelsAsync elapsed = {sw}", sw.Elapsed);
            return products;
        }

        private async Task<ProductViewModel> GetProductViewModelFromPathAsync(string path)
        {
            ProductViewModel product = null;
            var response = await _client.GetAsync(path);
            if (response.IsSuccessStatusCode)
            {
                product = await response.Content.ReadAsAsync<ProductViewModel>();
            }
            return product;
        }

        private async Task<IEnumerable<ProductViewModel>> GetProductViewModelsFromPathAsync(string path)
        {
            var products = new List<ProductViewModel>();
            var response = await _client.GetAsync(path);
            if (response.IsSuccessStatusCode)
            {
                products = await response.Content.ReadAsAsync<List<ProductViewModel>>();
            }
            return products;
        }

        //public async Task<ProductViewModel> GetByProductIdAsync(int productId)
        //{
        //    string path = $"api/product/{productId}";
        //    var product = await GetProductViewModelFromPathAsync(path);
        //    return product;
        //}

        //public async Task<ProductViewModel> GetByNameAsync(string name)
        //{
        //    string path = $"api/product/name/{name}";
        //    var product = await GetProductViewModelFromPathAsync(path);
        //    return product;
        //}

        // http://stackoverflow.com/questions/29319086/cancelling-an-httpclient-request-why-is-taskcanceledexception-cancellationtoke
        /* With cancellation token:
         * try
            {

                var cts = new CancellationTokenSource();

                ProductViewModel product = null;
                var responseTask = _client.GetAsync(path, cts.Token);
                //HttpResponseMessage response = await _client.GetAsync(path, cts.Token);

                // request cancellation
                cts.Cancel();

                HttpResponseMessage response = await responseTask;

                if (response.IsSuccessStatusCode)
                {
                    product = await response.Content.ReadAsAsync<ProductViewModel>();
                }
                return product;
            }
            catch (TaskCanceledException ex)
            {
                Console.WriteLine(ex);
                throw;
            }
         */

    }
}
