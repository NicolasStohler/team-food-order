﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;

namespace ConnString.Offline.Engine
{
    public class AutofacRegistrationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            // builder.RegisterType<AppConfig>().As<IAppConfig>().InstancePerLifetimeScope();

            builder.RegisterAssemblyTypes(ThisAssembly)
                .AsImplementedInterfaces();

        }
    }
}
