﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using ConnString.AppConfiguration;
using ConnString.Domain;
using ConnString.Offline.Domain;
using ConnString.ViewModels;
using EnsureThat;
using LibLog.Common.Logging;

namespace ConnString.Offline.Engine
{
    internal class ProductConversionEngine : IProductConversionEngine
    {
        private static readonly ILog Logger = LogProvider.GetCurrentClassLogger();

        private readonly IConnectionStringStoreConfig _connectionStringStoreConfig;
        private readonly IMapper _mapper;

        public ProductConversionEngine(IMapper mapper, IConnectionStringStoreConfig connectionStringStoreConfig)
        {
            _mapper = mapper;
            _connectionStringStoreConfig = connectionStringStoreConfig;
        }

        public OfflineProduct GetOfflineProductFromProduct(Product product)
        {
            if (product == null)
            {
                return null;
            }
            var op = _mapper.Map<OfflineProduct>(product);
            op.OfflineValidUntil = DateTime.Today.AddDays(_connectionStringStoreConfig.ValidOfflineDays);
            Ensure.That(op.Crc32, "op.Crc32").IsEqualTo(op.GetCalculatedCrc32());
            return op;

            //var op = new OfflineProduct()
            //{
            //    ProductId = product.ProductId,
            //    Name = product.Name,
            //    ConnectionString = product.ConnectionString,
            //    ValidFrom = product.ValidFrom,
            //    ValidTo = product.ValidTo,
            //    DbType = product.DbType,
            //    Crc32 = product.GetCrc32(),
            //    OfflineValidUntil = DateTime.Today.AddDays(_connectionStringStoreConfig.ValidOfflineDays),
            //};
            //Ensure.That(op.Crc32, "op.Crc32").IsEqualTo(op.GetCalculatedCrc32());
            //return op;
        }

        public OfflineProduct GetOfflineProductFromProductViewModel(ProductViewModel productVm)
        {
            if (productVm == null)
            {
                return null;
            }
            var op = _mapper.Map<OfflineProduct>(productVm);
            op.OfflineValidUntil = DateTime.Today.AddDays(_connectionStringStoreConfig.ValidOfflineDays);
            Ensure.That(op.Crc32, "op.Crc32").IsEqualTo(op.GetCalculatedCrc32());
            return op;

            //var p = new Product()
            //{
            //    ProductId = productVm.ProductId,
            //    Name = productVm.Name,
            //    ConnectionString = productVm.ConnectionString,
            //    ValidFrom = productVm.ValidFrom,
            //    ValidTo = productVm.ValidTo,
            //    DbType = productVm.DbType,
            //};
            //return CeateFromProduct(p, _connectionStringStoreConfig.ValidOfflineDays);
        }
    }
}
