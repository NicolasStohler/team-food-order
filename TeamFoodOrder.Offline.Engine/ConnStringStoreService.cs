﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConnString.AppConfiguration;
using ConnString.Offline.Domain;
using ConnString.ViewModels;
using EnsureThat;
using LibLog.Common.Logging;

namespace ConnString.Offline.Engine
{
    internal class ConnStringStoreService : IConnStringStoreService
    {
        private static readonly ILog Logger = LogProvider.GetCurrentClassLogger();

        private readonly IConnectionStringStoreConfig _connectionStringStoreConfig;
        private readonly IMasterStoreEngine _masterStoreEngine;
        private readonly IOfflineDbEngine _offlineDbEngine;
        private readonly IProductConversionEngine _productConversionEngine;

        public ConnStringStoreService(IMasterStoreEngine masterStoreEngine, IOfflineDbEngine offlineDbEngine,
            IConnectionStringStoreConfig connectionStringStoreConfig, IProductConversionEngine productConversionEngine)
        {
            _masterStoreEngine = masterStoreEngine;
            _offlineDbEngine = offlineDbEngine;
            _connectionStringStoreConfig = connectionStringStoreConfig;
            _productConversionEngine = productConversionEngine;
        }

        public async Task<string> GetConnectionStringAsync()
        {
            var op = await GetOfflineProductAsync();
            return op.ConnectionString;
        }

        private (Task<OfflineProduct> sqliteTask, Task<ProductViewModel> webApiTask, Task<IEnumerable<ProductViewModel>>
            webApiTaskRelatedOfflineProductsTask) StartTasks(string productName, string dbType, bool queryWebApi)
        {
            var sqliteTask = GetOfflineProductAsync(productName, dbType);    // this takes longer, so start it first!

            var webApiTask = queryWebApi
                ? _masterStoreEngine.GetProductViewModelAsync(productName, dbType)
                : null;
            var webApiTaskRelatedOfflineProductsTask = queryWebApi
                ? _masterStoreEngine.GetRelatedProductsViewModelsAsync(productName)
                : null;

            return (
                sqliteTask: sqliteTask, webApiTask: webApiTask, webApiTaskRelatedOfflineProductsTask:
                webApiTaskRelatedOfflineProductsTask);
        }

        public async Task<OfflineProduct> GetOfflineProductAsync()
        {
            var sw          = Stopwatch.StartNew();
            var productName = _connectionStringStoreConfig.ProductName;
            var dbType      = _connectionStringStoreConfig.DbType;
            var queryWebApi = _connectionStringStoreConfig.QueryWebApi;
            Logger.InfoFormat("GetOfflineProductAsync: {name} ({dbType})", productName, dbType);

            var tasks = StartTasks(productName, dbType, queryWebApi);   // start tasks: WebApi | SQLite in parallel

            var webApiProductViewModel = (queryWebApi) ? await tasks.webApiTask : null;
            var sqliteOfflineProduct = await tasks.sqliteTask;

            if (webApiProductViewModel == null && sqliteOfflineProduct == null)
            {
                throw new NoProductFoundException($"No connection string found for '{productName}'");
            }

            OfflineProduct webApiOfflineProduct = null;
            if (webApiProductViewModel != null)
            {
                webApiOfflineProduct =
                    _productConversionEngine.GetOfflineProductFromProductViewModel(webApiProductViewModel);
            }

            // process results
            OfflineProduct returnOfflineProduct = ProcessWebApiAndSqliteProducts(webApiProductViewModel,
                sqliteOfflineProduct, webApiOfflineProduct);
            
            if (sqliteOfflineProduct == null)
            {
                returnOfflineProduct =
                    await _offlineDbEngine.AddOrUpdateOfflineProductAsync(webApiOfflineProduct); // add or update
            }
            Ensure.That(returnOfflineProduct, "returnOfflineProduct").IsNotNull();

            await ProcessRelatedProducts(tasks.webApiTaskRelatedOfflineProductsTask);

            sw.Stop();
            Logger.InfoFormat("GetOfflineProductAsync result for {name} ({dbType}): {connStr} (elapsed: {sw})",
                returnOfflineProduct.Name, returnOfflineProduct.DbType, 
                returnOfflineProduct.ConnectionString, sw.Elapsed);
            return returnOfflineProduct;
        }

        private async Task ProcessRelatedProducts(Task<IEnumerable<ProductViewModel>> webApiTaskRelatedOfflineProductsTask)
        {
            var relatedProductViewModels = (webApiTaskRelatedOfflineProductsTask != null)
                ? await webApiTaskRelatedOfflineProductsTask
                : null;

            var relatedOfflineProducts =
                relatedProductViewModels?.Select(
                    pvm => _productConversionEngine.GetOfflineProductFromProductViewModel(pvm));

            if (relatedOfflineProducts != null)
            {
                await _offlineDbEngine.AddOrUpdateRelatedOfflineProductsAsync(relatedOfflineProducts);
            }
        }

        private OfflineProduct ProcessWebApiAndSqliteProducts(ProductViewModel webApiProductViewModel,
            OfflineProduct sqliteOfflineProduct, OfflineProduct webApiOfflineProduct)
        {
            OfflineProduct returnOffProduct = null;
            if (webApiProductViewModel != null && sqliteOfflineProduct != null)
            {
                bool offlineEntryStillValid = false;
                if (sqliteOfflineProduct != null)
                {
                    offlineEntryStillValid = IsOfflineEntryStillValid(sqliteOfflineProduct);
                }

                if (offlineEntryStillValid)
                {
                    var webCrc32 = webApiProductViewModel.GetCrc32();
                    var sqliteCrc32 = sqliteOfflineProduct.Crc32;
                    if (webCrc32 == sqliteCrc32)
                    {
                        returnOffProduct = sqliteOfflineProduct;
                    }
                }
            }
            else if (sqliteOfflineProduct != null)
            {
                returnOffProduct = sqliteOfflineProduct;
            }
            else // webApiProductViewModel != null
            {
                returnOffProduct = webApiOfflineProduct;
            }
            Ensure.That(returnOffProduct, "returnOffProduct").IsNotNull();
            return returnOffProduct;
        }

        private async Task<OfflineProduct> GetOfflineProductAsync(string productName, string dbType)
        {
            var sw = Stopwatch.StartNew();

            await _offlineDbEngine.EnsureDatabaseExistsAsync();
            var sqliteTask = await _offlineDbEngine.GetOfflineProductAsync(productName, dbType);

            sw.Stop();
            Logger.InfoFormat("ConnStringStoreService:GetOfflineProductAsync elapsed = {sw}", sw.Elapsed);
            return sqliteTask;
        }

        private bool IsOfflineEntryStillValid(OfflineProduct sqliteOfflineProduct)
        {
            Ensure.That(sqliteOfflineProduct, "sqliteOfflineProduct").IsNotNull();
            var diff = DateTime.Today - sqliteOfflineProduct.OfflineValidUntil;
            return diff.Days <= _connectionStringStoreConfig.ValidOfflineDays;
        }
    }
}