﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ConnString.Domain;
using ConnString.ViewModels;

namespace ConnString.Offline.Engine
{
    public interface IMasterStoreEngine
    {
        //Task<ProductViewModel> GetByProductIdAsync(int productId);
        //Task<ProductViewModel> GetByNameAsync(string name);
        Task<IEnumerable<string>> GetAllNamesAsync();

        Task<IEnumerable<ProductViewModel>> GetAllProductsAsync();
        Task<ProductViewModel> GetProductByProductIdAsync(int productId);
        Task<ProductViewModel> GetProductByNameAsync(string name);
        Task<ProductViewModel> GetProductViewModelAsync(string name, string dbType);
        Task<IEnumerable<ProductViewModel>> GetRelatedProductsViewModelsAsync(string name);
    }
}