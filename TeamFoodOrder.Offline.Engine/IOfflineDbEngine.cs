﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConnString.Offline.Domain;

namespace ConnString.Offline.Engine
{
    public interface IOfflineDbEngine
    {
        Task<OfflineProduct> GetOfflineProductAsync(string name, string dbType);
        Task<IEnumerable<OfflineProduct>> GetRelatedOfflineProductsAsync(string name);
        Task<OfflineProduct> AddOrUpdateOfflineProductAsync(OfflineProduct offlineProduct);
        Task EnsureDatabaseExistsAsync();
        Task AddOrUpdateRelatedOfflineProductsAsync(IEnumerable<OfflineProduct> relatedOfflineProducts);
    }
}
