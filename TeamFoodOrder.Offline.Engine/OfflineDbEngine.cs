﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConnString.Domain;
using ConnString.Offline.Data;
using ConnString.Offline.Domain;
using DisconnectedGenericRepository;
using Force.Crc32;
using LibLog.Common.Logging;

namespace ConnString.Offline.Engine
{
    internal class OfflineDbEngine : IOfflineDbEngine
    {
        private static readonly ILog Logger = LogProvider.GetCurrentClassLogger();

        //private readonly IGenericRepository<OfflineProduct> _offlineProductRepository;
        private readonly IOfflineProductRepository _offlineProductRepository;

        public OfflineDbEngine(IOfflineProductRepository offlineProductRepository)
        {
            _offlineProductRepository = offlineProductRepository;
        }

        public async Task<OfflineProduct> AddOrUpdateOfflineProductAsync(OfflineProduct offlineProduct)
        {
            // make sure there's no entry with the same productId already? 
            return await _offlineProductRepository.AddOrUpdateOfflineProductAsync(offlineProduct);
        }

        public async Task<OfflineProduct> GetOfflineProductAsync(string name, string dbType)
        {
            var sw = Stopwatch.StartNew();

            var offlineProduct = await GetWithNameFallbackAsync(name, dbType);

            sw.Stop();
            Logger.InfoFormat("OfflineDbEngine:GetOfflineProductAsync elapsed = {sw}", sw.Elapsed);

            return offlineProduct;
        }

        public async Task<IEnumerable<OfflineProduct>> GetRelatedOfflineProductsAsync(string name)
        {
            var sw = Stopwatch.StartNew();

            var offlineProducts = new List<OfflineProduct>();;
            var parts = name.Trim().Split('-');
            for (int useParts = parts.Count(); useParts > 0; useParts--)
            {
                var searchName = string.Join("-", parts.Take(useParts));
                var products = await GetRelatedByNameAsync(searchName);
                if (products != null)
                {
                    offlineProducts.AddRange(products);
                }
            }
            sw.Stop();
            Logger.InfoFormat("OfflineDbEngine:GetRelatedOfflineProductsAsync elapsed = {sw}", sw.Elapsed);

            return offlineProducts;
        }

        public async Task EnsureDatabaseExistsAsync()
        {
            var sw = Stopwatch.StartNew();

            await _offlineProductRepository.EnsureDatabaseExistsAsync();

            sw.Stop();
            Logger.InfoFormat("OfflineDbEngine:EnsureDatabaseExistsAsync elapsed = {sw}", sw.Elapsed);
        }

        public async Task AddOrUpdateRelatedOfflineProductsAsync(IEnumerable<OfflineProduct> relatedOfflineProducts)
        {
            await _offlineProductRepository.AddOrUpdateRelatedOfflineProductsAsync(relatedOfflineProducts);
        }

        private async Task<OfflineProduct> GetWithNameFallbackAsync(string name, string dbType)
        {
            // ext: name parsing: "iasibe2-formulare-frm123" 
            // 1: exists "iasibe2-formulare-frm123": yes => return like above
            // 2: exists "iasibe2-formulare": yes => return like above
            // 3: exists "iasibe2": yes => return like above
            // return null

            var parts = name.Trim().Split('-');
            for (int useParts = parts.Count(); useParts > 0; useParts--)
            {
                var searchName = string.Join("-", parts.Take(useParts));
                var product = await GetByNameDbTypeAsync(searchName, dbType);
                if (product != null)
                {
                    return product;
                }
            }

            return null;
        }

        private async Task<OfflineProduct> GetByNameDbTypeAsync(string name, string dbType)
        {
            var product = await _offlineProductRepository
                .GetOfflineProductAsync(name, dbType);
            return product;
        }

        private async Task<IEnumerable<OfflineProduct>> GetRelatedByNameAsync(string name)
        {
            var products = await _offlineProductRepository
                .GetRelatedOfflineProductsAsync(name);
            return products;
        }
    }
}
