﻿using ConnString.Domain;
using ConnString.Offline.Domain;
using ConnString.ViewModels;

namespace ConnString.Offline.Engine
{
    public interface IProductConversionEngine
    {
        OfflineProduct GetOfflineProductFromProduct(Product product);
        OfflineProduct GetOfflineProductFromProductViewModel(ProductViewModel productVm);
    }
}