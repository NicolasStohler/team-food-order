﻿namespace TeamFoodOrder.AppConfiguration
{
	public interface IDbContextConfig
	{
		bool AddLibLogLoggerProvider { get; }
		bool EnableSensitiveDataLogging { get; }
	}
}