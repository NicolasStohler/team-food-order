﻿using System;
using EcConfig.Core;
using LibLog.Common.Logging;

namespace TeamFoodOrder.AppConfiguration
{
    internal class ConnectionStringStoreConfig : IConnectionStringStoreConfig
    {
        private static readonly ILog Logger = LogProvider.GetCurrentClassLogger();
        private const string ConfigSection = "connection-string-store";

        public ConnectionStringStoreConfig()
        {
            Logger.DebugFormat("AppConfig.ConnectionStringStoreConfig ready! {@this}", this);
        }

        public string ProductName { get; } = Config.Get($"{ConfigSection}.ProductName");
        public string DbType { get; } = Config.Get($"{ConfigSection}.DbType");
        public int ValidOfflineDays { get; } = Convert.ToInt32(Config.Get($"{ConfigSection}.ValidOfflineDays"));

        public bool QueryWebApi { get; } = Convert.ToBoolean(Config.Get($"{ConfigSection}.QueryWebApi"));
        public string HttpClientBaseAddress { get; } = Config.Get($"{ConfigSection}.HttpClientBaseAddress");
        public int HttpClientTimeoutSeconds { get; } = Convert.ToInt32(Config.Get($"{ConfigSection}.HttpClientTimeoutSeconds"));
    }
}
