﻿using Autofac;

namespace TeamFoodOrder.AppConfiguration
{
	public class AutofacRegistrationModule : Module
	{
		protected override void Load(ContainerBuilder builder)
		{
			base.Load(builder);

			//builder.RegisterAssemblyTypes(ThisAssembly).AsImplementedInterfaces();

			builder.RegisterType<AppConfig>().As<IAppConfig>().InstancePerLifetimeScope();
			builder.RegisterType<DbContextConfig>().As<IDbContextConfig>().InstancePerLifetimeScope();
			builder.RegisterType<ConnectionStringStoreConfig>().As<IConnectionStringStoreConfig>().InstancePerLifetimeScope();
		}
	}
}
