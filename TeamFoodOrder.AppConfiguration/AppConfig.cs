﻿using System;
using EcConfig.Core;
using EnsureThat;
using LibLog.Common.Logging;

namespace TeamFoodOrder.AppConfiguration
{
	internal class AppConfig : IAppConfig
	{
		private static readonly ILog Logger = LogProvider.GetCurrentClassLogger();

		public AppConfig()
		{
			Logger.Info("Reading AppConfig (default.config)...");

			Ensure.That(MySetting, "MySetting").IsNotNullOrWhiteSpace();

			Logger.DebugFormat("AppConfig ready! {@this}", this);
		}

		public string MySetting { get; } = Config.Get("MySetting");
		public int MyValue { get; } = Convert.ToInt32(Config.Get("MyValue"));
		public DateTime MyDate { get; } = Convert.ToDateTime(Config.Get("MyDate"));

		// public bool EnableSensitiveDataLogging
		//public IDbContextConfig DbContextConfig { get; } = new DbContextConfig();	// own class, registered?
	}
}
