﻿namespace TeamFoodOrder.AppConfiguration
{
    public interface IConnectionStringStoreConfig
    {
        string ProductName { get; }
        string DbType { get; }
        int ValidOfflineDays { get; }
        int HttpClientTimeoutSeconds { get; }
        string HttpClientBaseAddress { get; }
        bool QueryWebApi { get; }
    }
}