﻿using System;

namespace TeamFoodOrder.AppConfiguration
{
	public interface IAppConfig
	{
		DateTime MyDate { get; }
		string MySetting { get; }
		int MyValue { get; }
	}
}