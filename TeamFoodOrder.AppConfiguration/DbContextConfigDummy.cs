﻿namespace TeamFoodOrder.AppConfiguration
{
	public class DbContextConfigDummy : IDbContextConfig
	{
		public bool EnableSensitiveDataLogging { get; } = false;
		public bool AddLibLogLoggerProvider { get; } = false;
	}
}
