﻿using System;
using EcConfig.Core;
using LibLog.Common.Logging;

namespace TeamFoodOrder.AppConfiguration
{
	internal class DbContextConfig : IDbContextConfig
	{
		private static readonly ILog Logger = LogProvider.GetCurrentClassLogger();
		private const string ConfigSection = "db-context";

		public DbContextConfig()
		{

			Logger.DebugFormat("AppConfig.DbContext ready! {@this}", this);
		}

		public bool AddLibLogLoggerProvider { get; } =
			Convert.ToBoolean(Config.Get($"{ConfigSection}.AddLibLogLoggerProvider"));

		public bool EnableSensitiveDataLogging { get; } =
			Convert.ToBoolean(Config.Get($"{ConfigSection}.EnableSensitiveDataLogging"));
	}
}
