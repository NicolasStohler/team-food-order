﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using EnsureThat;
using LibLog.Common.Logging;

namespace ConnString.PreBootstrapper
{
    public class PreBootstrapper
    {
        private static readonly ILog Logger = LogProvider.GetCurrentClassLogger();

        private static IContainer _autofacContainer = null;

        public static IContainer Initialize(Action<ContainerBuilder> containerBuilderFn = null)
        {
            Logger.Info("Bootstrapper.Initialize");

            Ensure.That(_autofacContainer == null, "_autofacContainer").IsTrue();

            _autofacContainer = PreBootstrapper.Initialize_internal(builder =>
            {
                // invoke containerBuilderFn if provided
                containerBuilderFn?.Invoke(builder);

                AutoMapperConfig.RegisterForDI(builder);
            });

            Ensure.That(_autofacContainer, "_autofacContainer").IsNotNull();

            return _autofacContainer;
        }

        private static IContainer Initialize_internal(Action<ContainerBuilder> containerBuilder)
        {
            return AutofacLoader.Initialize(containerBuilder);
        }
    }
}
