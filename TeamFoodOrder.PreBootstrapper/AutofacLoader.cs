﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;

namespace ConnString.PreBootstrapper
{
    public static class AutofacLoader
    {
        public static IContainer Initialize(Action<ContainerBuilder> containerBuilderFn)
        {
            IContainer container = null;
            ContainerBuilder builder = new ContainerBuilder();

            // register types here

            // using Autofac-Modules:
            builder.RegisterAssemblyModules(typeof(Offline.Data.AutofacRegistrationModule).Assembly);
            builder.RegisterAssemblyModules(typeof(Offline.Engine.AutofacRegistrationModule).Assembly);
            builder.RegisterAssemblyModules(typeof(AppConfiguration.AutofacRegistrationModule).Assembly);

            // invoke containerBuilderFn if provided
            containerBuilderFn?.Invoke(builder);

            container = builder.Build();

            return container;
        }
    }
}
