﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using AutoMapper;
using ConnString.Domain;
using ConnString.Offline.Domain;
using ConnString.ViewModels;

namespace ConnString.PreBootstrapper
{
    public class AutoMapperConfig
    {
        public static void RegisterForDI(ContainerBuilder builder)
        {
            // http://stackoverflow.com/a/35208535/54159
            builder.Register(ctx => AutoMapperConfig.GetConfig());
            builder.Register(ctx => ctx.Resolve<MapperConfiguration>().CreateMapper()).As<IMapper>();
        }

        private static MapperConfiguration GetConfig()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<UncheckedAutoMapperConfig>();        // no call to AssertConfigurationIsValid below!
                cfg.AddProfile<CheckedAutoMapperConfig>();
            });
            config.AssertConfigurationIsValid(typeof(CheckedAutoMapperConfig).FullName);

            return config;
        }
    }

    internal class CheckedAutoMapperConfig : Profile
    {
        public CheckedAutoMapperConfig()
        {
            CreateMap<Product, OfflineProduct>()
                .ForSourceMember(s => s.IsDirty, o => o.Ignore())
                .ForMember(d => d.OfflineProductId, o => o.Ignore())
                .ForMember(d => d.Crc32, o => o.MapFrom(s => s.GetCrc32()))
                .ForMember(d => d.OfflineValidUntil, o => o.Ignore())
                ;

            CreateMap<ProductViewModel, OfflineProduct>()
                .ForSourceMember(s => s.IsFallBackMatch, o => o.Ignore())
                .ForSourceMember(s => s.RequestedName, o => o.Ignore())
                .ForMember(d => d.OfflineProductId, o => o.Ignore())
                .ForMember(d => d.Crc32, o => o.MapFrom(s => s.GetCrc32()))
                .ForMember(d => d.OfflineValidUntil, o => o.Ignore())
                ;
        }
    }

    internal class UncheckedAutoMapperConfig : Profile
    {
        public UncheckedAutoMapperConfig()
        {
        }
    }
}
